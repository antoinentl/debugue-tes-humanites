---
# Modèle de métadonnées pour une séance

{{/*
  Exemple d'input attendu pour le nommage de fichier:

  $ hugo new posts/seance-02-comprendre-les-formats-avec-pandoc.md

  Le résultat pour le fichier ainsi créé avec la commande `hugo new ...` générera le titre suivant:
    "Comprendre Les Formats Avec Pandoc"
  
  Deux expressions régulières sont utilisées pour extraire le titre:

    1. Le numéro de séance est variable (utilisé plus loin, pour métadonnée `seance`).
    2. Intitulé de la séance. Il est grossièrement extrait de ce qui suit le numéro.
*/ -}}
# intitulé de la présentation
# @type chaîne de caractères libre
title: "{{ humanize (replace (replaceRE "seance-([0-9]{2})-(.+)" "$2" .Name) "-" " ") }}"

# courte description
# chaîne de caractères libre
description: ""

# date de modification de la page (pour Hugo)
# @type chaîne de caractères contenant une date (format strict: "2006-01-02")
date: {{ .Date | dateFormat "2006-01-02" }}

# heure de la présentation
# @type chaîne de caractères libre
heure_p: ""

# lien de présentation (laisser vide pour "à venir" ou `false` pour aucun lien)
# @type booléen (false) | chaîne de caractères (vide, url)
lien: "" # https://url-visio.example...

# lieu, nom du local
# @type chaîne de caractères libre
lieu: "Bibliothèque des lettres et sciences humaines, local 0000"

# liste des compétences acquises (phrases complètes)
# @type liste<chaîne de caractères>
competences:
- "Comprendre [ex. le fonctionnement d'un ordinateur]."

# liste des formatrice·teur·s de la séance
# @type liste<chaîne de caractères>
formatrices:
- Louis-Olivier Brassard
- Giulia Ferretti

{{/*
  Expression régulière pour extraire le numéro de séance du chemin.

  Pour un chemin:

    posts/saison-04/seance-02-titre-de-seance.md
  
  On fait l'extraction en deux temps:
    1. Extraction du numéro ("02") qui suit le mot "seance-" dans "../seance-02-....md"
*/ -}}
# numéro de la séance
# @type chaîne de caractères
seance: "{{ replaceRE "seance-([0-9]{2})-(.+)" "$1" .Name }}"

{{/*
  Expression régulière pour extraire la saison du chemin.

  Pour un chemin:

    posts/saison-04/seance-02-titre-de-seance.md
  
  On fait l'extraction en deux temps:
    1. Extraction du numéro ("04") dans "saison-04/....md"
    2. Retrait du préfixe zéro, s'il y en a un. Ex. "04" => 4.
*/ -}}
# saison, pour assurer l'archivage
# @type numéro
saison: {{ replaceRE "^0?([0-9])$" "$1" (replaceRE "(.+)/saison-([0-9]{1,2})(.+)" "$2" .File.Path) }}

# Si la séance doit être listée ou non. La page sera tout de même générée.
# Notez que cela est différent de l'option `published` de Hugo,
# cette dernière permettant de régler la génération ou non de la page comme telle.
# @type booléen (true|false)
visible: false

# disposition style diapositives, voire `layouts/_default/diapositive.html`
# @type chaîne de caractères
layout: diapositive

# si les diapositives doivent figurer sur la page
# @type booléen (true|false)
afficher_diapositives: true

# si la boîte de présentation doit être ouverte par défaut ou non
# @type booléen (true|false) 
diapositives_ouvertes: false

# vignette de couverture (à fabriquer séparément, voire README.md)
# décommenter les deux lignes suivantes lorsque l'image a été générée
# @type liste
#images:
#- /images/feature/{{ replaceRE "(.+)/saison-([0-9]{1,2})(.+)" "saison-$2" .File.Path }}/{{ .File.BaseFileName }}.png
---

<!-- Le corps de texte va ici. -->

<!-- Baliser les diapositives avec les shortcodes `{{< psectioni >}}` pour ouvrir et `{{< psectiono >}}` pour fermer. -->

{{< psectioni >}}

{{< psectiono >}}
