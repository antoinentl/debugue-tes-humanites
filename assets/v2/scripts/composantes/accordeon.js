// init lorsque la page est chargée
document.addEventListener('DOMContentLoaded', initTousLesAccordeons, false);

//////////////

/** @type {HTMLAllCollection} (À transformer en {Array}) */
let accordeons;

/**
 * Attacher les événements pour les accordéons sur la page
 */
function initTousLesAccordeons() {
  // aller chercher tous les accordéons
  accordeons = document.querySelectorAll('.accordeon');
  // transformer `accordeons` en type {Array} pour avoir accès à toutes les méthodes
  accordeons = Array.from(accordeons);

  accordeons.forEach(accordeon => {
    let _entete = accordeon.querySelector('.accordeon__entete');
    let _bouton = accordeon.querySelector('.accordeon__bouton');
    let _ctrl = { estReduit: false };
    _ctrl.estReduit = !accordeon.getAttribute('aria-expanded') || false;

    _bouton.addEventListener('click', () => {
      if (_ctrl.estReduit) {
        // on doit afficher le contenu
        accordeon.setAttribute('aria-expanded', true);
        _ctrl.estReduit = !_ctrl.estReduit;
      } else {
        // on doit réduire le contenu
        accordeon.removeAttribute('aria-expanded');
        _ctrl.estReduit = !_ctrl.estReduit;
      }
    });

    // marquer le bouton comme "actif" (pour les styles)
    _bouton.classList.add('js-actif');
    
  });
}