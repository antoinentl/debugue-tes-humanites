---
title: Débogue tes humanités
---

Établir une réflexion en sciences humaines et sociales aujourd'hui signifie comprendre et manipuler des outils *d’écriture numérique*.
Il est nécessaire de proposer des formations dans les institutions de savoir pour former les chercheur·e·s en sciences humaines aux théories et pratiques numériques.

**Nouveau cycle 2023-2024 :** la quatrième saison aura lieu à partir du <mark>10 octobre 2023</mark>, environ trois fois par trimestre, les mardis de 13h à 15h (heure de Montréal).
Ces rencontres ouvertes et gratuites pour tous et toutes se feront en présence (sur inscription via le [site web des bibliothèques](https://bib.umontreal.ca/formations/calendrier)) et en ligne, par visioconférence. 
<!-- Vous pouvez rejoindre la formation à l'adresse suivante : <https://meet.jit.si/DebugHumanitesCRCEN-BLSH>. -->
