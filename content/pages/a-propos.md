---
title: À propos
date: 2023-09-26
---

Ce site regroupe les contenus de la formation <a href="/" rel="self" class="debugue">Débogue tes humanités</a>, organisée en premier lieu par la <a href="https://www.ecrituresnumeriques.ca/fr/" title="Consulter le site web" rel="external noreferrer" target="_blank">Chaire de recherche du Canada sur les écritures numériques</a> à l'automne 2021. Depuis l'automne 2022, <a href="/" rel="self" class="debugue">Débogue tes humanités</a> s'est étendu afin de diffuser les connaissances à un public plus large. Pour ce faire, nous travaillons désormais en partenariat avec la Bibliothèque de lettres et sciences humaines de l'Université de Montréal, ainsi que l'Ouvroir d'histoire de l'art et de muséologie numérique.

La troisième saison de <a href="/" rel="self" class="debugue">Débogue tes humanités</a> propose deux parties : un parcours orienté littératie numérique et un parcours plus avancé.
Pour consulter les séances, rendez-vous à la section [archives](/archives).

Les contenus sont placés sous la licence CC-BY-NC-SA.

Contacts : 

Giulia Ferretti <giulia.ferretti@umontreal.ca> \
Louis-Olivier Brassard <louis-olivier.brassard@umontreal.ca>
