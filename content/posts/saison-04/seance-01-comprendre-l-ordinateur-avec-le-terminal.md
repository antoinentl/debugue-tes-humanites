---
# Modèle de métadonnées pour une séance

# intitulé de la présentation
# @type chaîne de caractères libre
title: "Comprendre l’ordinateur avec le terminal"

# courte description
# chaîne de caractères libre
description: "Pourquoi est-ce important de savoir comment les fichiers de son ordinateur sont organisés ?  Plusieurs stratégies permettent de les manipuler plus facilement, et même créer des liens si besoin. Nous proposerons une douce introduction au terminal."

# date de modification de la page (pour Hugo)
# @type chaîne de caractères contenant une date (format strict: "2006-01-02")
date: 2023-10-10

# heure de la présentation
# @type chaîne de caractères libre
heure_p: "13h-15h"

# lien de présentation (laisser vide pour "à venir" ou `false` pour aucun lien)
# @type booléen | chaîne de caractères
lien: "https://meet.jit.si/DebogueHumanitesCRCEN-BLSH" # https://url-visio.example...

# lieu, nom du local
# @type chaîne de caractères libre
lieu: "Bibliothèque des lettres et sciences humaines, local 3091"

# liste des compétences acquises (phrases complètes)
# @type liste<chaîne de caractères>
competences:
- "Connaître l'architecture des fichiers dans l'ordinateur"
- "Renommer et déplacer des fichiers à partir du terminal"
- "Comprendre le potentiel du terminal"

# liste des formatrice·teur·s de la séance
# @type liste<chaîne de caractères>
formatrices:
- Marcello Vitali-Rosati
- Louis-Olivier Brassard
- Giulia Ferretti

# numéro de la séance
# @type chaîne de caractères
seance: "01"

# saison, pour assurer l'archivage
# @type numéro
saison: 4

# Si la séance doit être listée ou non. La page sera tout de même générée.
# Notez que cela est différent de l'option `published` de Hugo,
# cette dernière permettant de régler la génération ou non de la page comme telle.
# @type booléen (true|false)
visible: true

# disposition style diapositives, voire `layouts/_default/diapositive.html`
# @type chaîne de caractères
layout: diapositive

# si les diapositives doivent figurer sur la page
# @type booléen (true|false)
afficher_diapositives: true

# si la boîte de présentation doit être ouverte par défaut ou non
# @type booléen (true|false) 
diapositives_ouvertes: false

# vignette de couverture (à fabriquer séparément)
images:
- /images/feature/saison-04/seance-01-comprendre-l-ordinateur-avec-le-terminal.png
---

<!-- Le corps de texte va ici. -->

<!-- Baliser les diapositives avec les shortcodes `{{< psectioni >}}` pour ouvrir et `{{< psectiono >}}` pour fermer. -->

{{< psectioni >}}
## Plan de la séance

1. Tour de table et d'écrans
2. Déroulement de la formation
3. Origines de l'informatique
4. Principes du numérique
5. Qu'est-ce qu'un programme/logiciel ?
6. Usages basiques d'un terminal

{{< pnote >}}
Pour cette première séance nous vous proposons de découvrir les enjeux de cette formation ainsi que les différentes séances au programme.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## 1. Tour de table et d'écrans

3 questions :

- votre nom
- ce que vous faites cette année
- l'objet numérique le plus proches de vous (autre que votre ordinateur)
{{< psectiono >}}


{{< psectioni >}}
## 2. Déroulement de la formation

- des séances théoriques et pratiques
- des échanges
- vous pouvez amener vos projets
- des supports et de la documentation en ligne
{{< psectiono >}}

{{< psectioni >}}
## 3. Origines de l'informatique
### Des calculateurs analogiques aux machines programmables
- première machine à calculer : boulier (antiquité) ;
- ordinateur : capacité de faire des calculs sans intervention humaine ;
- 1936 : basculement dans l'histoire de l'informatique ;
- machine de Turing.

{{< pnote >}}
Si l'informatique est une science récente, et un ensemble de technologies développées au 20e siècle, il faut garder à l'esprit que l'informatique née dès l'antiquité, soit 4000 ans avant notre ère. Le père de l'ordinateur c'est la machine à calculer : un projet qui démarre pendant l'antiquité avec le boulier, et qui se concrétise plus tard avec les inventions de Pascal ou de Leibniz. La grande différence entre ces prémisses et l'ordinateur réside dans la computation : l'ordinateur peut exécuter des programmes, c'est-à-dire enchaîner une suite logique de calculs (une procédure de calcul) sans intervention humaine.

![Machine de Leibniz](/images/leibnizCalculator.jpeg)

La date de 1936 est un basculement dans l'histoire de l'informatique : Alan Turing publie un article fondateur sur la calculabilité, qui résout un problème fondamental de logique, qui passera à l'époque inaperçu auprès de celles et ceux qui travaillent sur les machines à calculer. En 1936 c'est aussi une époque où les états se réarment, et beaucoup d'efforts sont mis sur la cryptographie pour sécuriser les moyens de communication, d'où ce besoin de calculateurs.

[La machine de Turing](https://fr.wikipedia.org/wiki/Machine_de_Turing) est une machine conceptuelle. Elle n'existe pas en tant que telle, il s'agit simplement un modèle pour penser le principe de l'informatique. Il n'y a par exemple pas de différence entre un ordinateur d'aujourd'hui et une machine de Turing. On parle alors de " machine universelle ", car elle traite l'information de façon simple. La machine de Turing permet de faire n'importe quel calcul, elle traite l'information de façon universelle. Alan Turing est un personnage emblématique dans l'histoire de l'informatique (et plus globalement dans l'histoire des sciences et des techniques).
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Origines de l'informatique
{{< /pcache >}}
### Qu'est-ce qu'un ordinateur ?

- ceci n'est pas une boîte noire ;
- distinguer le matériel (*hardware*) du logiciel (*software*) ;
- support + message.

Tout d'abord il faut écarter l'idée que l'ordinateur serait une boîte noire, ou une machine dont le comportement serait aussi incompréhensible
qu'imprévisible. L'informatique s'est fortement complexifiée depuis une trentaine d'années, sans parler du fait que la plupart des terminaux
sont désormais connectés à Internet, mais ce n'est pas pour cela qu'il faut considérer un ordinateur comme une chose mystérieuse.

Cela ne veut pas pour autant dire que je pourrais vous expliquer simplement comment fonctionne un ordinateur, mais déjà les distinctions que Michel Serres vous a présenté sont utiles :

- il s'agit de la composition de deux éléments (hardware et software)
- et de l'association d'un support et d'un message (pour le dire vite).
{{< psectiono >}}

{{< psectioni >}}
## 4. Principes du numérique
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
{{< /pcache >}}
### Le numérique au sens propre du terme

Représentation de la réalité via des éléments discrets et atomiques qui correspondent à des nombres naturels.

S'oppose à analogique: représentation du réel via un signal continu, "analogue" au réel.
{{< psectiono >}}

{{< psectioni >}}
![Numérique et analogique](http://www.bedwani.ch/electro/ch23/image88.gif)
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
{{< /pcache >}}
### La modélisation du monde

1. modèle conceptuel
    - donner une description en langage naturel de quelque chose
2.  modèle computationnel
    - transformer la représentation en unités atomiques discrètes et
        définir des fonctions pour les traiter\
        Le "*numérique*" se situe ici!
3.  modèle matériel
    - implémenter le calcul fonctionnel dans une machine de Turing
        réelle.
    - calculable = computable

Les trois étapes ne sont pas étanches! 
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique 
{{< /pcache >}}
### La base 2

Pour pouvoir implémenter l'approche numérique dans une machine avec **2** symboles disponibles (plein vide, noir/blanc, +/-...).

| Base 10   | Base 2 |
|-----------| -------|
|  0        | 0      |
|  1        | 1      |
|  2        | 10     |
|  3        | 11     |
|  4        | 100    |
|  5        | 101    |
|  6        | 110    |

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
{{< /pcache >}}

### La base 2

| Base 10   | Base 2 |
|-----------|--------|
| 8         | 1000   |
| 9         | 1001   |
| 10        | 1010   |
| 11        | 1011   |
| 12        | 1100   |
| 13        | 1101   |
| 14        | 1110   |
| 15        | 1111   |

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
{{< /pcache >}}
### La base 2


- 11 en base 10 signifie: 1 dizaine et 1 unité (10+1).
- 11 en base 2 signifie: 1 couple et une unité ( et donc en base 10:
    2+1=3)
- en base 10 avec 4 chiffres je peux exprimer: 10^4^ = 10x10x10x10 =
    10000 chiffres (en effet de 0 à 9999)
- en base 2 avec 4 chiffres je peux exprimer 2^4^ = 2x2x2x2=16
- en base 2 avec 8 chiffres je peux exprimer 2^8^ = 256 (un octet)
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
{{< /pcache >}}

### Les algorithmes


Série finie et exacte d'instructions permettant de résoudre un problème ou d'effectuer des calculs, à partir des données initiales (input). Les algorithmes respectent deux conditions:

- à chaque instruction il est possible de connaître l'instruction suivante
- si on suit les étapes on arrive à une instruction qui demande l'arrêt (output)

{{< pnote >}}
Un algorithme qui s'exécute indéfiniment est un erreur de logique.
Par exemple :

{{< highlight "python" >}}
# python

# Variables
pommes = 1
cerises = 2

# Attention! La boucle ne s'arrêtera jamais!
while pommes + cerises > 1
    pommes = 3 - pommes
    cerises = 3 - cerises
end
{{< /highlight >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
{{< /pcache >}} 
### La machine de Turing

[Jouez avec une machine de Turing virtuelle](https://interstices.info/comment-fonctionne-une-machine-de-turing/)
{{< psectiono >}}

{{< psectioni >}}
## 5. Qu'est-ce qu'un programme/logiciel ?

- programme informatique : suite d'instructions qu'exécute un
    ordinateur
- logiciel : ensemble de programmes informatiques + interfaces
- les conditions d'utilisation
{{< pnote >}}
Un *algorithme* est une procédure de calcul compréhensible par l'humain et pas nécessairement par la machine, un *programme* est une procédure de calcul compréhensible par la machine.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## 6. Usages basiques d'un terminal
### 6.1. Qu'est-ce qu'un terminal ?

> Un terminal est une interface graphique (*graphical user interface*, ou GUI) qui émule une console. Il nous permet d’exécuter un shell.

— [Source](https://doc.ubuntu-fr.org/terminal)

{{< pnote >}}
Un terminal est un moyen d'interagir
avec un ordinateur.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.2 Définition : le shell
Le shell est un interpréteur de commandes. Il s’agit d’un programme qui transmet les commandes entrées par l’utilisateur au système d’exploitation pour qu’il les exécute.

Des exemples de programmes shell sont `bash` (qui est aussi un langage de commande) et `zsh` (Z shell). 
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.3 L'intérêt
Les autres interfaces graphiques nous offrent une série d’options et nous orientent vers un parcours d’action. Grâce au terminal, nous pouvons construire nos propres commandes, adaptées à nos besoins.
En apprenant à bien utiliser le terminal, nous pouvons exécuter des commandes complexes très rapidement. 
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.4. Ouvrir un terminal
Sous Linux ou Mac, chercher « Terminal » parmi les **applications**.

Sous Windows, chercher « Windows Power Shell » ; dans le menu déroulant en haut sélectionner « Ubuntu » ; écrire dans le terminal :

{{< highlight "shell" "linenos=false" >}}
# Windows PowerShell

cd /c/Users/votre-nom-sur-Windows
{{< /highlight >}}

Sinon suivre [ces instructions](https://korben.infoinstaller-shell-bash-linux-windows-10.html)
ou
[celles-ci](https://blog.ineat-group.com/2020/02/utiliser-le-terminal-bash-natif-dans-windows-10/).
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal 
{{< /pcache >}}
### 6.5. Savoir où l'on est

{{< highlight "shell" "linenos=false" >}}
pwd

# exemple de réponse: /home/nom-d-utilisateur
{{< /highlight >}}

À noter ici qu'il s'agit d'un **chemin absolu** puisque l'adresse/chemin
indiquée commence par une barre oblique `/`.
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.6. Lister les fichiers

{{< highlight "shell" "linenos=false" >}}
ls
{{< /highlight >}}

ou

{{< highlight "shell" "linenos=false" >}}
ls -a
{{< /highlight >}}

pour voir aussi les fichiers cachés (dont le nom débute par un `.`).

Pourquoi vouloir voir les fichiers cachés ? Ce sera utile pour la suite.
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.7. Naviguer

Pour changer de dossier, utiliser la commande `cd`, suivi du chemin. Exemple :

{{< highlight "shell" "linenos=false" >}}
cd Document/Photos
{{< /highlight >}}

mène au dossier `Photos` dans le dossier `Documents`.

{{< pnote >}}
Il est possible d'indiquer à la fois un chemin relatif, donc sans
commencer par une barre oblique, ou en commençant par une barre oblique
pour un chemin absolu.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal  
{{< /pcache >}}
### 6.8. Les bons réflexes

- touche `TAB` pour compléter une commande
- flèche du haut `↑` pour parcourir les dernières commandes utilisées
- `CTRL + R` pour rechercher une commande en tapant les premières lettres (faites à nouveau `CTRL + R` autant de fois que nécessaire pour parcourir l'historique à partir des lettres que vous avez tapées)
- `CTRL + L` ou commande `clear` pour repartir sur un nouveau terminal 
{{< psectiono >}}


{{< psectioni >}}

{{< pcache >}}
## 6. Usages basiques d'un terminal  
{{< /pcache >}}
### Redimensionner des images en masse

Nous avons plusieurs dizaines de photographies en très haute résolution.
Du fait de leur résolution élevée (plusieurs mégapixels), la taille de fichier est également très élvée.

Pour ne pas alourdir le document numérique dans lequel nous souhaitons les incorporer (comme un diaporama ou un document PDF), il faudrait les **redimensionner** (par exemple, passer de `6000px` sur le long côté à au plus `600px`).

Comment faire?

{{< psectiono >}}

{{< psectioni >}}

### Une première solution

Il serait possible d'importer chaque image dans un logiciel de traitement d'image (avec une interface graphique) et de renommer le fichier ainsi transformé.

Toutefois, pour une masse de plusieurs dizaines images, cela s'avérerait fastidieux et chronophage.

Cette tâche triviale pourrait être facilement automatisée grâce à une seule (!) instruction en ligne de commande.

{{< psectiono >}}

{{< psectioni >}}

### Une autre solution, en ligne de commande

Avec le logiciel [ImageMagick](https://imagemagick.org/) (attention, il s'agit d'un logiciel *sans interface graphique !*), on pourrait utiliser l'utilitaire `convert` qu'il propose :

{{< highlight "bash" "linenos=false" >}}
#!/bin/bash

convert Photo1.jpg Photo1.redim.jpg -resize 600
{{< /highlight >}}

Résultat : on obtiendrait ainsi un fichier `Photo1.redim.jpg` qui ferait 600 px sur le plus long côté, sans écraser l'image originale.

{{< pcache >}}
Examinons la commnade !
{{< /pcache >}}

{{< psectiono >}}

{{< psectioni >}}

Décortiquons la commande :

- `convert` : la **commande** que nous souhaitons exécuter. Il faudra l'accompagner d'*arguments*, un peu comme les paramtètres *x*, *y*, ... d'une fonction mathématique.
- `Photo1.jpg` : **premier argument**. Pour la commande `convert`, il s'agit du fichier en entrée, qu'on souhaitera convertir.
- `Photo1.redim.jpg` : **deuxième argument**. Il s'agit du fichier en sortie, résultant de la transformation, qu'on a choisi de renommer avec le suffixe `.redim`.
- `-resize 600` : il s'agit d'une **option** (ou « drapeau ») qui précise la conversion. Ici, le drapeau `-resize` indique que la largeur maximale sur le plus long côté doit être d'au plus 600 px. Il faut toujours se référer à la documentation du logiciel pour connaître les options disponibles. Il est d'ailleurs courant d'avoir plusieurs drapeaux pour une seule commande (pour une image, on pourrait également spécifier, en plus de la résolution maximale, le ratio, la compression, la palette de couleurs, etc.).

Là encore, on pourrait procéder une image à la fois... Ce qui n'est guère plus viable dès lors qu'on a plusieurs fichiers à redimensionner.
Ne pourrait-on pas gagner du temps ?

{{< psectiono >}}

{{< psectioni >}}

### Généraliser la ligne de commande

Grâce aux connaissances acquises dans la section précédente, nous pourrons, au saupoudrant un petit peu de `bash`, généraliser notre commande.

Considérant un dossier dans lequel il y a plusieurs images dont l'extension est `.jpg` :

{{< highlight "shell" "linenos=false" >}}
# Dans un répertoire ~/projet/avec/images

Photo1.jpg
Photo2.jpg
Photo3.jpg
Photo_x.jpg
# et ainsi de suite...
{{< /highlight >}}

{{< psectiono >}}

{{< psectioni >}}

...on pourrait recourir à une **boucle** (pour chaque image `.jpg`, effectuer le redimensionnement avec le suffixe `.jpg`) :

{{< highlight "bash" "linenos=false" >}}
#!/bin/bash

for i in *.jpg; do
  convert $i.jpg $i.redim.jpg -resize 600
done
{{< /highlight >}}

**Résultat** : chaque image a été redimensionnée sans écraser l'originale !

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.9. Gérer des fichiers ou des dossiers

- `touch` mon-fichier.txt
- `mkdir` mon-dossier.txt
- `grep -ri mot-recherche` pour trouver tous les fichiers contenant le terme qui nous intéresse
- `find . -type f -name "*.txt"` pour trouver tous les fichiers qui se terminent par l'extension `.txt`

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.10. Déplacer un fichier

La commande `mv` permet de **renommer** ou de **déplacer** un fichier (dans le terminal, c'est la même chose!).
Deux exemples :

{{< highlight "shell" "linenos=false" >}}
# Pour renommer le fichier "mon-fichier.md" => "fichier.md"

mv mon-fichier.md fichier.md
{{< /highlight >}}

{{< highlight "shell" "linenos=false" >}}
# Pour déplacer le fichier "mon-fichier.md" dans le dossier "Documents/"
# Attention! On suppose que ce dossier existe déjà

mv /home/utilisateur/mon-fichier.md /home/utilisateur/Documents/mon-fichier.md
{{< /highlight >}}

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.11. Supprimer un fichier 

{{< highlight "shell" "linenos=false" >}}
# Pour supprimer uniquement un fichier
rm mon-fichier.md

# L'option -R permet de supprimer les sous-répertoires, récursivement.
# (Par défaut, la commande `rm` refusera de supprimer les sous-répertoires,
# au cas où on aurait oublié qu'il y en avait d'autres dans ce dossier.)
rm -R mon-dossier
{{< /highlight >}}

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.12. Afficher le contenu d'un fichier 

{{< highlight "shell" "linenos=false" >}}
cat mon-fichier.md
{{< /highlight >}}

Avec les logiciels `nano` ou `vim` :

{{< highlight "shell" "linenos=false" >}}
# avec nano
nano mon-fichier.md

# avec vim
vim mon-fichier.md
{{< /highlight >}}

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.13. Exercice
1. créez un dossier `livre` contenant un sous-dossier `chapitre01`, contenant lui-même un fichier `texte.txt`
2. naviguez dans le dossier `chapitre01`
3. revenez dans le dossier `livre`
4. déplacez le fichier `texte.txt` dans le dossier `livre`
5. renommez le fichier `texte.txt` dans le dossier `livre`
6. supprimez le dossier `chapitre01`

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.14. Pour en savoir plus sur une commande de terminal
 
{{< highlight "shell" "linenos=false" >}}
man nom-commande
{{< /highlight >}}
{{< psectiono >}}

