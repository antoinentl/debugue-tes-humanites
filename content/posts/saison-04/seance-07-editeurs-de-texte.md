---
# Modèle de métadonnées pour une séance

# intitulé de la présentation
# @type chaîne de caractères libre
title: "Éditeurs de texte (VSCode, Zettlr, Stylo)"

# courte description
# chaîne de caractères libre
description: "Nous vous présenterons comment convertir vos fichiers en plusieurs formats, en automatisant ce passage pour ne pas perdre d’informations. Typiquement, vous serez en mesure de transformer un fichier HTML en fichier PDF, en prenant en compte toutes les données contenues dans la source."

# date de modification de la page (pour Hugo)
# @type chaîne de caractères contenant une date (format strict: "2006-01-02")
date: 2024-03-12

# heure de la présentation
# @type chaîne de caractères libre
heure_p: "13h-15h"

# lien de présentation (laisser vide pour "à venir" ou `false` pour aucun lien)
# @type booléen | chaîne de caractères
lien: "https://meet.jit.si/DebogueHumanitesCRCEN-BLSH"

# lieu, nom du local
# @type chaîne de caractères libre
lieu: "Bibliothèque des lettres et sciences humaines, local 2074"

# liste des compétences acquises (phrases complètes)
# @type liste<chaîne de caractères>
competences:
- Utiliser Stylo, Zettler et VSCode pour la rédaction de textes structurés 
- Structurer les données du texte en YAML et BibTeX dans l'éditeur de texte Stylo
- Conversion entre formats grâce à l’éditeur de texte Stylo
- Comprendre l’intérêt des éditeurs de texte WYSIWYM (*What You See Is What You Mean*)

# liste des formatrice·teur·s de la séance
# @type liste<chaîne de caractères>
formatrices:
- Louis-Olivier Brassard
- Giulia Ferretti

# numéro de la séance
# @type chaîne de caractères
seance: "07"

# saison, pour assurer l'archivage
# @type numéro
saison: 4

# Si la séance doit être listée ou non. La page sera tout de même générée.
# Notez que cela est différent de l'option `published` de Hugo,
# cette dernière permettant de régler la génération ou non de la page comme telle.
# @type booléen (true|false)
visible: true

# disposition style diapositives, voire `layouts/_default/diapositive.html`
# @type chaîne de caractères
layout: diapositive

# si les diapositives doivent figurer sur la page
# @type booléen (true|false)
afficher_diapositives: true

# si la boîte de présentation doit être ouverte par défaut ou non
# @type booléen (true|false) 
diapositives_ouvertes: true

# vignette de couverture (à fabriquer séparément, voire README.md)
# décommenter les deux lignes suivantes lorsque l'image a été générée
# @type liste
images:
- /images/feature/saison-04/seance-07-editeurs-de-texte.png
---

<!-- Le corps de texte va ici. -->

<!-- Baliser les diapositives avec les shortcodes `{{< psectioni >}}` pour ouvrir et `{{< psectiono >}}` pour fermer. -->

{{< psectioni >}}

{{< pcache >}}

## Plan de la séance

0. Introduction
1. Éditeurs de texte vs. logiciels de traitement de texte
2. Stylo : un exemple d'éditeur de texte en ligne
3. VSCode/VSCodium : un éditeur de texte avec IDE
4. Pandoc pour convertir vos documents en Markdown
5. Aller plus loin... Vim et Nano

{{< /pcache >}}

{{< psectiono >}}

{{< psectioni >}}

## 0. Introduction 

Un éditeur de texte est un programme qui permette d'éditer du **texte brut** (éventuellement avec du balisage). Les éditeurs de texte sont donc nécessaires à la fois pour écrire des documents lisibles pour nous (c'est l'usage le plus commun en sciences humaines) et pour écrire des programmes informatiques. Des éditeurs de texte minimalistes sont intégrés dans presque tous les systèmes d'exploitation, mais il en existe des centaines, des plus simples aux plus complexes.

{{< psectiono >}}

{{< psectioni >}}

{{< figure src="/images/system-360-terminal.png" caption="L'éditeur de texte intégré à l'ordinateur System 360">}}

{{< psectiono >}}

{{< psectioni >}}

Quelques exemples :  

- éditeurs de texte « simples » : Vim, Nano 
- éditeurs de texte pour écrire en Markdown : Zettler, Stylo
- éditeurs de texte avec IDE (Integrated Development Environment) : VSCode/VSCodium, Sublime Text

Attention : la notion d'éditeur de texte n'est pas nécessairement liée à celle d'interface. Les premiers éditeurs de texte étaient de simples lignes de commande qui permettaient à la machine d'exécuter des instructions.  

{{< psectiono >}}

{{< psectioni >}}

## 1. Éditeurs de texte vs. logiciels de traitement de texte

Logiciel de traitement de texte :
- supporte l'édition et la mise en forme des documents (par exemple, il divise le texte en plusieurs pages)
- permet l'inclusion de médias.
- la structure sémantique du texte est souvent confondue avec la structure de l'information.

Éditeur de texte : 
- supporte l'édition du texte et de son balisage, donc de sa structure sémantique.
- certains éditeurs de texte proposent une coloration sémantique pour faciliter l'édition.
- certains logiciels combinent des éditeurs de texte avec une série d'applications et de fonctionnalités pour faciliter la rédaction du code informatique.

{{< psectiono >}}

{{< psectioni >}}


<div style="display: grid; grid-template-columns: repeat(2, 1fr); gap: .5rem; align-items: last baseline;">

{{< figure src="/images/2024-03-12-helix-exemple.png" caption="Exemple d'un éditeur de texte très minimaliste">}}

{{< figure src="/images/2024-03-12-libre-office-exemple.png" caption="Exemple d'un logiciel de traitement de texte">}}

</div>

{{< psectiono >}}

{{< psectioni >}}

## 2. Stylo : un exemple d'éditeur de texte en ligne

Stylo est un éditeur de texte en ligne avec des fonctionnalités supplémentaires, spécialement conçu pour la publication de textes scientifiques dans le domaine des sciences humaines et sociales.

Il est aussi un projet de recherche développé par la Chaire de recherche du Canada sur les humanités numériques.

- Créé en 2017 par Marcello Vitali-Rosati et son équipe
- Au cœur de la chaîne éditoriale de plusieurs revues scientifiques ([Nouvelles Vues](https://nouvellesvues.org/), [Sens Public](https://www.sens-public.org/), [Humanités numériques](https://revue-humanites-numeriques.humanisti.ca/index.php/hn), [Fémur](https://revuefemur.com/index.php/la-revue/), [Lampadaire](https://lampadaire.ca/))

La documentation de Stylo est disponible [ICI](https://stylo-doc.ecrituresnumeriques.ca/fr/)

{{< psectiono >}}

{{< psectioni >}}

### 2.1. La « Stylo-sophie »

1. Établir une réflexion sur la relation entre les environnements d'écriture et l'écriture scientifique
2. Remettre les auteurs et éditeurs en charge de la structuration sémantique du texte
3. Encourager l'adoption de formats et de standards ouverts pour l'écriture scientifique (Markdown, BibTex pour la bibliographie, YAML pour les métadonnées)

Pour un rappel des principes et de la syntaxe de Markdown, voir la documentation de la [**troisième séance**](https://debogue.ecrituresnumeriques.ca/) de cette saison de Débogue tes humanités.

{{< psectiono >}}


{{< psectioni >}}

### 2.2. Stylo : tour de la plateforme

- Création et gestion des articles
- Gestion des étiquettes (tags)
- Partage des articles
- Export les articles
- Prévisualisation les article

{{< psectiono >}}

{{< psectioni >}}

{{< figure src="/images/2024-03-12-stylo-tour-plateforme.gif" caption="Stylo : tour de la plateforme">}}

{{< psectiono >}}


{{< psectioni >}}

### 2.3. Stylo : édition d'un article

- Édition du contenu principal
- Édition des métadonnées
- Édition de la bibliographie et des citations
- Gestion des versions

{{< psectiono >}}

{{< psectioni >}}

{{< figure src="/images/2024-03-12-stylo-edition-article.gif" caption="Stylo : édition d'un article">}}

{{< psectiono >}}

{{< psectioni >}}

### 2.4. Stylo en dehors de Stylo

- Prévisualisation et annotation
- Export

{{< psectiono >}}

{{< psectioni >}}

### 2.5. Nouvelles fonctionnalités

- Espaces de travail
- Écriture collaborative
- API GraphQL

{{< psectiono >}}

{{< psectioni >}}

### 2.6. Des autres éditeurs de texte en Markdown

- [Zettler](https://www.zettlr.com/)
- [Obsidian](https://obsidian.md/)
- [Typora](https://typora.io/) 
- [iA Writer](https://ia.net/writer)

{{< psectiono >}}

{{< psectioni >}}

## 3. VSCode/VSCodium : un éditeur de texte avec IDE

- Annoncée par Microsoft en 2015 lors de la conférence Build (la conférence annuelle organisée par Microsoft)
- [Le code source](https://github.com/microsoft/vscode) est sous licence MIT et disponible sur GitHub
- En tant que produit téléchargeable, VSCode n'est pas un logiciel libre et contient des systèmes de télémétrie (suivi de l'utilisation et des performances des logiciels)
- [VSCodium](https://vscodium.com/) est une alternative totalement libre à VSCode

{{< psectiono >}}

{{< psectioni >}}

{{< figure src="/images/2024-03-12-vscode.png" caption="VSCodium : aperçu">}}

{{< psectiono >}}

{{< psectioni >}}

### 3.2. Tour de la plateforme

- Gestion de fichier et des dossier
- Terminal intégré
- Compilation automatique du code
- Monaco, le même éditeur de texte que stylo (`ctrl + H `« tous replacer » et possibilité d'effectuer des recherches par RegEx)

{{< psectiono >}}

{{< psectioni >}}

### 3.3. Les principes des VSCode

- Une interface relativement simple avec des fonctionnalités très avancées pour faciliter la programmation (complétion de code et débogage)
    > « Less time fiddling with your environment, and more executing your ideas »
- Beaucoup de raccourcis clavier pour travailler plus vite 
- Personnalisation (par exemple : `file > Preferences > Theme > Color Theme` pour changer le thème)
- Communauté d'utilisateurs

{{< psectiono >}}


{{< psectioni >}}

## 4. Pandoc pour convertir vos documents en Markdown

Conversion du format docx en Markdown à l'aide de Pandoc, avec la commande :

`pandoc -f docx -t markdown "mon-fichier.docx" -o "mon-fichier.md`

Pour accéder à une alternative temporaire et comprendre les commandes Pandoc : [Try pandoc](https://pandoc.org/try/)

Pour un rappel sur comment télécharger et utiliser Pandoc voir la [séance de Débogue tes humanités dédiée aux formats numériques](https://debogue.ecrituresnumeriques.ca/saison-04/seance-02-les-formats/#5-pandoc-en-pratique)

{{< psectiono >}}


{{< psectioni >}}

## 5. Aller plus loin... Vim et Nano

[Nano](https://www.nano-editor.org/) ou [Vim](https://www.vim.org/) à partir du terminal

- `vim mon-fichier.md` ou `nano mon-fichier.md` pour ouvrir des documents dans Vim ou Nano
- `i` pour éditer un document avec Vim. `:wq` pour sauver et fermer le document

{{< figure src="/images/2024-03-12-vim.png" caption="Vim : aperçu">}}

{{< psectiono >}}







