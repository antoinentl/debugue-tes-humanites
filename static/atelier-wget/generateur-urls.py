#!/usr/bin/python
# -*- coding: utf-8 -*-

# Liste des urls (séparées par des sauts de ligne `\n`),
# qu'on va écrire ligne par ligne dans le fichier `urls.txt`.
urls = '';

# L'URL de base, avec un jeton `%d` à remplacer par un numéro de
# folio. Le suffixe `.medres` permet d'avoir une image en
# résolution moyenne, bien suffisant pour nos besoins.
# (On pourrait aussi utiliser les extensions `.highres` ou `.lowres`)
baseUrl = 'https://gallica.bnf.fr/ark:/12148/btv1b86108277/%d.medres';

# on ouvre le fichier `liste-urls.txt` pour pouvoir écrire dedans pendant
# la boucle (ne pas oublier de le fermer à la fin)
fichier = open('liste-urls.txt', 'w')

# Itération sur un intervalle de pages avec la variable `x`
# ici, les folios 13 à 23
for x in range(13, 24):
    # on crée une URL à partir de l'itération, en remplaçant le jeton `%d`
    # par la valeur actuelle de `x`
    url = baseUrl % (x)
    # et on ajoute un saut de ligne à la fin de chaque URL lorsqu'on écrit
    # dans le fichier
    fichier.write(url + '\n')

# Lorsque la boucle est terminée, on ferme le fichier
fichier.close
