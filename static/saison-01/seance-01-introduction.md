+++
title = "Séance 01 - Introduction"
date = "2021-09-10"
date_p = "2021-09-10"
description = "Introduction à la formation"
layout = "archives-diapositive"
visible = true
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.5acfvmp4/c7d7ade01ae498758a64320e90eb5e4fd14c90a4"
saison = "1"
+++
{{< psectioni >}}
## Bienvenue !

1. Tour d'écrans
2. Déroulement de la formation
3. Exemples de projets

{{< pnote >}}
Pour cette première séance nous vous proposons de découvrir les enjeux de cette formation ainsi que les différentes séances au programme.
{{< /pnote >}}

{{< psectiono >}}

{{< psectioni >}}
## 1. Tour d'écrans
3 questions :

1. votre nom
2. ce que vous faites cette année
3. l'objet _numérique_ le plus proche de vous (autre que votre ordinateur)

{{< psectiono >}}

{{< psectioni >}}
## 2. Déroulement de la formation

- des séances théoriques et pratiques
- des échanges
- vous pouvez amener vos projets
- des supports et de la documentation en ligne

{{< pnote >}}

{{< /pnote >}}

{{< psectiono >}}

{{< psectioni >}}
## 3. Exemples de projets

{{< pnote >}}
À découvrir pendant la séance !
{{< /pnote >}}

{{< psectiono >}}
