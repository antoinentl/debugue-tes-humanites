+++
title = "Séance 02 - L'informatique en 3 points"
date = "2021-09-11"
date_p = "2021-09-17"
description = "Origines de l'informatique, principes du numérique et qu'est-ce qu'un programme."
layout = "archives-diapositive"
visible = true
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.b843tc46/a83c61b0e22f4eb99bcf7ad69ba2ef27b0241fc8"
saison = "1"
+++
{{< psectioni >}}
## Plan de la séance

1. Origines de l'informatique
2. Principes du numérique
3. Qu'est-ce qu'un programme ?

{{< psectiono >}}

{{< psectioni >}}
## 1. Origines de l'informatique
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 1. Origines de l'informatique
{{< /pcache >}}
### Des calculateurs analogiques aux machines programmables

- première machine à calculer : boulier (antiquité) ;
- ordinateur : capacité de faire des calculs _sans_ intervention humaine ;
- 1936 : basculement dans l'histoire de l'informatique ;
- machine de turing.

{{< pnote >}}
Si l'informatique est une science récente, et un ensemble de technologies développées au 20e siècle, il faut garder à l'esprit que l'informatique née dès l'antiquité, soit 4000 ans avant notre ère.
Le père de l'ordinateur c'est la machine à calculer : un projet qui démarre pendant l'antiquité avec le boulier, et qui se concrétise plus tard avec les inventions de Pascal ou de Leibniz.
La grande différence entre ces prémisses et l'ordinateur réside dans l'automatisme : l'ordinateur peut faire des calculs sans intervention humaine (il y a une absence de mécanique).

La date de 1936 est un basculement dans l'histoire de l'informatique : Alan Turing publie un article fondateur sur la calculabilité, qui résout un problème fondamental de logique, qui passera à l'époque inaperçu auprès de celles et ceux qui travaillent sur les machines à calculer.
En 1936 c'est aussi une époque où les états se réarment, et beaucoup d'efforts sont mis sur la cryptographie pour sécuriser les moyens de communication, d'où ce besoin de calculateurs.

[La machine de Turing](https://fr.wikipedia.org/wiki/Machine_de_Turing) est une machine conceptuelle, très basique.
Elle n'existe pas en tant que telle, il s'agit simplement un modèle pour penser le principe de l'informatique.
Il n'y a par exemple pas de différence entre un ordinateur d'aujourd'hui et une machine de Turing.
On parle alors de "machine universelle", car elle traite l'information de façon simple.
La machine de Turing permet de faire n'importe quel calcul, elle traite l'information de façon universelle.
Alan Turing est un personnage emblématique dans l'histoire de l'informatique (et plus globalement dans l'histoire des sciences et des techniques).
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 1. Origines de l'informatique
{{< /pcache >}}
### Qu'est-ce qu'un ordinateur ?

- ceci n'est pas une boîte noire ;
- distinguer le matériel (_hardware_) du logiciel (_software_) ;
- support + message.

{{< pnote >}}
Tout d'abord il faut écarter l'idée que l'ordinateur serait une boîte noire, ou une machine dont le comportement serait aussi incompréhensible qu'imprévisible.
L'informatique s'est fortement complexifiée depuis une trentaine d'années, sans parler du fait que la plupart des terminaux sont désormais connectés à Internet, mais ce n'est pas pour cela qu'il faut considérer un ordinateur comme une chose mystérieuse.

Cela ne veut pas pour autant dire que je pourrais vous expliquer simplement comment fonctionne un ordinateur, mais déjà les distinctions que Michel Serres vous a présenté sont utiles :

- il s'agit de la composition de deux éléments (hardware et software)
- et de l'association d'un support et d'un message (pour le dire vite).

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## 2. Principes du numérique

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 2. Principes du numérique
{{< /pcache >}}
### Le numérique au sens propre du terme
Représentation de la réalité via des éléments discrets et atomiques qui correspondent à des nombres naturels.

S’oppose à analogique: représentation du réel via un signal continu, "analogue" au réel.
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 2. Principes du numérique
### Le numérique au sens propre du terme
{{< /pcache >}}
![Numérique et analogique](http://www.bedwani.ch/electro/ch23/image88.gif)
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 2. Principes du numérique
{{< /pcache >}}
### La modélisation du monde
Trois étapes:

1. modèle représentationnel
   - donner une description en langage naturel de quelque chose
2. modèle fonctionnel
   - transformer la représentation en unités atomiques discrètes et définir des fonctions pour les traiter   
    Le "_numérique_" se situe ici!
3. modèle physique
   - implémenter le calcul fonctionnel dans une machine de Turing réelle.
   - calculable = computable

{{< pnote >}}
Les trois étapes ne sont pas étanches!
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 2. Principes du numérique
{{< /pcache >}}
### La base 2

Pour pouvoir implémenter l'approche numérique dans une machine avec **2** symboles disponibles (plein/vide, noir/blanc, +/-...).

|Base 10|Base 2|
|---------|---------|
|0|0|
|1|1|
|2|10|
|3|11|
|4|100|
|5|101|
|6|110|
|7|111|

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Principes du numérique
### La base 2
{{< /pcache >}}
|Base 10|Base 2|
|---------|---------|
|8|1000|
|9|1001|
|10|1010|
|11|1011|
|12|1100|
|13|1101|
|14|1110|
|15|1111|

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 2. Principes du numérique
### La base 2
{{< /pcache >}}

- 11 en base 10 signifie: 1 dizaine et 1 unité (10+1).
- 11 en base 2 signifie: 1 couple et une unité ( et donc en base 10: 2+1=3)
- en base 10 avec 4 chiffres je peux exprimer: 10<sup>4</sup> = 10x10x10x10 = 10000 chifres (en effet de 0 à 9999)
- en base 2 avec 4 chiffres je peux exprimer 2<sup>4</sup> = 2x2x2x2=16
- en base 2 avec 8 chiffres je peux exprimer 2<sup>8</sup> = 256 (un octet)
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Principes du numérique
{{< /pcache >}}
### Les algorithmes
Ensemble d’instructions qui respectent deux conditions:

- à chaque instruction il est possible de connaître l’instruction suivante
- si on suit les étapes on arrive à une instruction qui demande l’arrêt
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Principes du numérique
{{< /pcache >}}
### La machine de Turing

[Jouez avec une machine de Turing virtuelle](https://interstices.info/comment-fonctionne-une-machine-de-turing/)
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Qu'est-ce qu'un programme/logiciel ?
{{< /pcache >}}
- programme informatique : suite d'instructions qu'exécute un ordinateur
- logiciel : ensemble de programmes informatiques + interfaces
- les conditions d'utilisation
{{< psectiono >}}