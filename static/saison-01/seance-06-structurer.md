+++
title = "Séance 06 - Structurer du texte"
date = "2021-09-11"
date_p = "2021-10-15"
description = "Le problème des traitements de texte, baliser le texte, les langages de balisage."
layout = "archives-diapositive"
visible = true
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.2c5fhg64/d358d3ae2bfe245394123509d3d37f7cfcd0fe73"
saison = "1"
+++
{{< psectioni >}}
## Plan de la séance

1. Le problème des traitements de texte
2. Structurer le texte
3. Baliser le texte
4. Les langages de balisage
{{< psectiono >}}


{{< psectioni >}}
## 1. Le problème des traitements de texte

- des logiciels de bureautique
- WYSIWYG
- confusion entre structure et mise en forme

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Structurer le texte

- la sémantique
- se repérer dans le texte
- automatiser le traitement

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Baliser le texte

- pour indiquer : une balise
- pour qualifier : un schéma
- **des** formats

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. Les langages de balisage

- pas légers : SGML, XML, HTML
- extensibles : XML
- des degrés de légèreté

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}
