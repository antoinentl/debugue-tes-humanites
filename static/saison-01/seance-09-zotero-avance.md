+++
title = "Séance 09 - Utilisation avancée de Zotero"
date = "2021-09-11"
date_p = "2021-11-19"
description = "Organiser sa bibliographie, les groupes, BetterBibTeX."
layout = "archives-diapositive"
visible = true
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.fc5ds4pj/45e4778771090edfeec89c1f567ed6c88ebf9c6a"
saison = "1"
+++
{{< psectioni >}}
## 4. La recherche avancée

{{< image src="zotero-recherche-avancee.png" >}}

{{< pnote >}}
Vous pouvez rechercher dans toutes vos références en cliquant sur la loupe, vous accédez alors à une interface de recherche avancée.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 5. Création de bibliographies
### 5.1. Comment créer une bibliographie ?

1. le style bibliographique
2. plusieurs milliers de styles : [https://www.zotero.org/styles](https://www.zotero.org/styles)
3. exemple : "Université de Montréal - APA (French - Canada)"
4. création d'une bibliographie : statique ou dynamique

{{< pnote >}}
- pour citer une référence ou créer une bibliographie, il faut utiliser un style ou une norme&nbsp;;
- il existe de nombreux styles de bibliographie ([https://www.zotero.org/styles](https://www.zotero.org/styles)), dont certains sont déjà intégrés à Zotero&nbsp;;
- par défaut je vous conseille d'utiliser le style suivant&nbsp;: "Université de Montréal - APA (French - Canada)"&nbsp;;
- pour modifier le style par défaut de Zotero&nbsp;: Édition > Préférences > Citer&nbsp;;
- chaque référence ou groupe de références peut être récupéré sous différents formats pour être intégré dans un document via un traitement de texte&nbsp;;
- plus d'informations&nbsp;: https://www.zotero.org/support/fr/creating_bibliographies
- _démonstration en cours_.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 5. Création de bibliographies
{{< /pcache >}}
### 5.2. Citer et créer une bibliographique directement dans un traitement de texte
Citations et bibliographies dynamiques dans Word ou LibreOffice.

{{< pnote >}}
Zotero peut s'intégrer dans des outils comme les traitements de texte.
Cela vous permet de citer des références et de créer des bibliographie plus facilement :

- ouvrez LibreOffice et vérifiez que vous disposez bien des options de Zotero (dans la partie supérieure gauche) ;
- si vous n'avez pas l'extension Zotero pour LibreOffice, installez là via le logiciel Zotero : Édition > Préférences > Citer > Traitements de texte > Installer le module LibreOffice
- vous pouvez ajouter une citation d'une référence préalablement enregistrée dans Zotero ;
- une fois des citations ajoutées, vous pouvez créer une bibliographie de ces citations ;
- si vous modifier vos références dans Zotero, vous pouvez mettre à jour votre document LibreOffice.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 6. Création de compte
### 6.1. Fonctionnalités proposées avec un compte

- synchronisation
- sauvegarde
- interface web
- afficher ses références
- etc.

{{< pnote >}}
Créer un compte sur Zotero permet plusieurs fonctionnalités&nbsp;:

- synchroniser ses références avec un compte en ligne&nbsp;: elles sont sauvegardées et disponibles en se connectant depuis n'importe quel ordinateur&nbsp;;
- utiliser l'interface web pour ajouter ou modifier vos références (moins pratique qu'avec le logiciel)&nbsp;;
- rendre public et visible vos références pour les partager avec d'autres personnes&nbsp;;
- échanger des références et en importer depuis d'autres comptes&nbsp;;
- créer ou participer à des groupes&nbsp;;
- pour créer un compte (gratuit, aucun message envoyé par Zotero)&nbsp;: https://www.zotero.org/user/register
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 7. Création et gestion de groupes
### 7.1. À quoi sert un groupe ?

- partager des références
- ouvrir les contributions
- afficher les contributions

{{< pnote >}}
Un groupe est une collection à laquelle plusieurs personnes peuvent participer&nbsp;:

- ajouter des références pour les rassembler dans un même groupe&nbsp;;
- plusieurs personnes peuvent contribuer à un même groupe&nbsp;;
- plusieurs rôles dans un même groupe&nbsp;: consulter, créer, modifier, administrer&nbsp;;
- le groupe peut être public avec des permissions plus ou moins ouvertes&nbsp;;
- _un groupe est facilement accessible via l'API de Zotero_.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 8. BibTeX
BibTeX est un format de fichier structurant une bibliographie :

- format qui a été créé en 1985 pour gérer les bases de données bibliographiques dans des fichier LaTeX ;
- le format BibTeX fonctionne par entrée bibliographiques : chaque entrée correspond à une description d’un document avec des items “mot-clef = valeur”.
{{< psectiono >}}


{{< psectioni >}}
```
@book{kirschenbaum_track_2016,
  address = {{Cambridge, Massachusetts, Etats-Unis d'Am{\'e}rique}},
  title = {Track Changes: A Literary History of Word Processing},
  isbn = {978-0-674-41707-6},
  shorttitle = {Track Changes},
  language = {anglais},
  publisher = {{The Belknap Press of Harvard University Press, 2016}},
  author = {Kirschenbaum, Matthew G.},
  year = {2016, cop. 2016}
}
```
{{< psectiono >}}


{{< psectioni >}}
## 9. Better BibTeX
### 9.1. Installation de Better BibTex
Better BibTex (https://retorque.re/zotero-better-bibtex/installation/) ajoute à Zotero des fonctionnalités très pratiques :

- affichage des clés de citation ;
- nouveaux formats d'export ;
- exports dynamiques (mises à jour automatiques) de bibliographies.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 9. Better BibTeX
{{< /pcache >}}
### 9.2. Installez Better BibTeX
[https://retorque.re/zotero-better-bibtex/installation/](https://retorque.re/zotero-better-bibtex/installation/)
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 9. Better BibTeX
{{< /pcache >}}
### 9.3. Exercices

1. créez une collection avec plusieurs références que vous exportez au format BetterBibTeX avec l'option "Garder à jour"
2. ajouter une référence à cette collection
3. est-ce que le fichier BibTeX a été modifié ?
4. verrouillez une clé de citation d'une référence de ce export
5. modifiez les formats de clé, que s'est-il passé dans votre fichier BibTeX ?
{{< psectiono >}}


{{< psectioni >}}
## 10. Exercice final

1. indiquez l'adresse utilisée pour votre compte Zotero dans l'espace de discussion instantanée
2. une fois que vous avez été ajouté·e au groupe `debugue-tes-humanites`, vérifiez qu'il s'affiche bien dans Zotero
3. ajoutez plusieurs références dans la collection `test`
4. verrouillez la clé de citation d'une ou plusieurs de vos références
5. créez une collection et ajoutez-y une référence bibliogaphique

{{< psectiono >}}
