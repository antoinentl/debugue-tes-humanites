+++
title = "Séance 10 - Imprimer des documents"
date = "2021-09-11"
date_p = "2021-11-26"
description = "Introduction à LaTeX, manipulations, Paged.js."
layout = "archives-diapositive"
visible = true
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.a20c3o3o/dd5a66a65373836e69aaa3db5628a181a298cf1b"
saison = "1"
+++
{{< psectioni >}}
## Plan de la séance

1. Introduction à LaTeX
2. Créer un document avec LaTeX via Pandoc
3. Présentation de Paged.js
4. Exemples de documents produits avec Paged.js
{{< psectiono >}}


{{< psectioni >}}
## 1. Introduction à LaTeX
langage de balisage + un système de composition = mettre en forme des documents

{{< pnote >}}
LaTeX est un puissant système de programmation éditoriale.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Introduction à LaTeX
{{< /pcache >}}
Distinction entre :

- le langage de balisage
- le système de composition
- la distribution

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Introduction à LaTeX
{{< /pcache >}}
Fonctionnement :

- rédaction via un éditeur de texte ou de code, balisage écrit en texte brut
- code source compilé par LaTeX pour produire un fichier au format PDF
- pour modifier le document il faut modifier le code source

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Introduction à LaTeX
{{< /pcache >}}
LaTeX sans LaTeX ?

Utilisation de Markdown comme balisage, Pandoc comme convertisseur et LaTeX comme processeur PDF.

(Lire [The LaTeX fetish (Or: Don’t write in LaTeX! It’s just for typesetting)](http://www.danielallington.net/2016/09/the-latex-fetish/))

{{< psectiono >}}


{{< psectioni >}}
## 2. Créer un document avec LaTeX via Pandoc

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Créer un document avec LaTeX via Pandoc
{{< /pcache >}}
Installation de LaTeX :

- Linux/Debian : `sudo apt install texlive-full`
- MacOS : https://www.tug.org/mactex/
- Windows : https://www.latex-project.org/get/

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Créer un document avec LaTeX via Pandoc
{{< /pcache >}}
Avant de produire un document :

- créer un document Markdown
- découvrir le template LaTeX par défaut : `pandoc -D latex`
- vérifier votre version de LaTeX : `latex -v`

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Créer un document avec LaTeX via Pandoc
{{< /pcache >}}
Produire un document :

`pandoc -s mon-document.md -o mon-document.pdf`

Pour voir le format intermédiaire TeX :

`pandoc mon-document.md -o mon-document.tex`

`pandoc mon-document.md -s -o mon-document.tex`

{{< psectiono >}}


{{< psectioni >}}
## 3. Présentation de Paged.js
Imprimer avec les outils du Web.

{{< pnote >}}
Aussi puissant que LaTeX ? Non, mais plus souple, et plus cohérent en ce qui concerne la séparation fond/forme.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Présentation de Paged.js
{{< /pcache >}}
> Paged.js is a free and open source JavaScript library that paginates content in the browser to create PDF output from any HTML content. This means you can design works for print (eg. books) using HTML and CSS!  
> [www.pagedjs.org/about/](https://www.pagedjs.org/about/)

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Présentation de Paged.js
{{< /pcache >}}
Qu'est-ce que Paged.js ?

- HTML + CSS + Paged.js = web to print
- fabriquer avec le Web

{{< psectiono >}}


{{< psectioni >}}
## 4. Exemples de documents produits avec Paged.js


{{< psectiono >}}
