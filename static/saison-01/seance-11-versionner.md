+++
title = "Séance 11 - Versionner les fichiers"
date = "2021-09-11"
date_p = "2021-12-10"
description = "Introduction à Git, exemples de projets, manipulations."
layout = "archives-diapositive"
visible = true
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.8b0a6825/ed023a8b292d0b06b850a266bbb56c8f8898b91a"
saison = "1"
+++
{{< psectioni >}}
## Plan de la séance

1. Pourquoi versionner ?
2. Décentraliser pour mieux travailler
3. Introduction à Git
4. Quelques projets qui utilisent Git
5. Manipulations
{{< psectiono >}}


{{< psectioni >}}
## 1. Pourquoi versionner ?
`mon-fichier-v2-relu-2021-12-09-final-ok-okok.txt`

{{< pnote >}}
Le versionnement (_versioning_ en anglais) entend répondre à plusieurs objectifs :

- enregistrer un ou plusieurs fichiers
- garder une trace des versions des fichiers
- naviguer dans l'historique des versions
- (le tout à plusieurs)

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Décentraliser pour mieux travailler
Plusieurs principes inhérents à une bonne gestion collective de fichiers informatiques :

- ne pas centraliser la sauvegarde
- _cloner_ facilement = copier le dossier de travail avec l'ensemble de l'historique
- travailler hors connexion
- créer des versions _parallèles_ d'un projet

{{< pnote >}}
Il y a eu de nombreux systèmes ou logiciels de gestion de versions, mais tous avaient un ou plusieurs défauts :

- la nécessité d'être connecté pour travailler
- ne pas pouvoir disposer de tous les fichiers sur sa propre machine
- centraliser les fichiers à un seul endroit
- etc.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Introduction à Git
Un logiciel de gestion de versions parmi d'autres (**pour les fichiers en plein texte**).

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< imageg src="git-for-humans.png" >}}
{{< psectiono >}}


{{< psectioni >}}
{{< imageg src="git-purr-01.jpg" >}}
{{< psectiono >}}


{{< psectioni >}}
{{< imageg src="git-purr-02.jpg" >}}
{{< psectiono >}}


{{< psectioni >}}
{{< imageg src="git-purr-03.jpg" >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. Quelques projets qui utilisent Git

- programmes informatiques : https://github.com/gohugoio/hugo/
- documentation : https://github.com/jgm/pandoc/tree/master/doc
- projets éditoriaux : https://github.com/thegetty/romanmosaics
- création (littéraire ?) : https://scolaire.loupbrun.ca/piece01/

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 5. Manipulations

- dans un dossier `mon-dossier`
- créer un fichier mon-fichier.md
- `git init`
- `git status`
- `git add mon-fichier.md`
- `git commit -m "le message d'enregistrement"`
- `git log`
- (`git fetch`, `git rebase` et `git push`)

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}
