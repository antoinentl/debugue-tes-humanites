+++
title = "Séance 02 - Des éditeurs de code et de texte"
date = "2022-01-05"
date_p = "2022-01-28"
description = "Nano, Vim, EMACS et VSCodium, exploration des possibilités offertes par des approches différentes."
layout = "archives-diapositive"
visible = true
saison = "2"
+++
{{< psectioni >}}
## Plan

1. Qu'est-ce qu'un éditeur de texte/de code ?
2. Distinction éditeur de texte et IDE
3. Les éditeurs "simples" : exemple de Nano
4. Les éditeurs "avancés" : exemple de Vim
5. Les IDE graphiques : Atom ou VSCode/VSCodium
6. Les éditeurs de texte Markdown

{{< psectiono >}}


{{< psectioni >}}
## 1. Qu'est-ce qu'un éditeur de texte/de code ?

- un programme/logiciel qui permet de lire et de modifier des fichiers au format texte (brut)
- **ce n'est pas un traitement de texte**
- le texte et rien que le texte (balisé)
- la coloration syntaxique en option
- l'interface graphique plus ou moins développée

{{< pnote >}}

{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Qu'est-ce qu'un éditeur de texte/de code ?
{{< /pcache >}}
Ce que n'est pas un éditeur de texte/de code :

- un outil d'import de format
- un convertisseur de formats
- un gestionnaire de métadonnées
- etc.

{{< pnote >}}

{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
## 2. Distinction éditeur de texte et IDE

- éditeur de texte : lire et modifier du _texte_
- IDE (integrated development environment) : ensemble d'outils dont un éditeur de texte

{{< pnote >}}
Il existe aussi des Web IDE : tout se passe dans le navigateur. GitLab propose par exemple une interface complète pour disposer d'un éditeur de texte en ligne ainsi que d'outils comme le versionnement.
{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Distinction éditeur de texte et IDE
{{< /pcache >}}
### Les fonctionnalités possibles d'un IDE

- une interface graphique avancée
- la coloration syntaxique : reconnaître les balises selon le format (Markdown, HTML, Python, XML, etc.)
- moteur de recherche : selon le format de balisage ou le langage de programmation
- un environnement de test
- une gestion des versions (intégration de Git par exemple)

{{< pnote >}}

{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
## 3. Les éditeurs "simples" : exemple de Nano

- un éditeur dans le terminal
- **pas de bouton**
- des raccourcis clavier pour interagir

{{< pnote >}}
L'avantage est de conserver le même environnement que celui du terminal.

Autre avantage : c'est un éditeur disponible dans beaucoup de situation sans avoir besoin d'installer des logiciels complexes et gourmands.
{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
{{< imageg src="editeur-01.png" legende="Capture d'écran de l'éditeur de texte Nano" >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. Les éditeurs "avancés" : exemple de Vim

- toujours dans le terminal
- des fonctions avancées pour réduire la friction
- des systèmes d'extension
- Vim ou Emacs peuvent être considérés comme des IDE (ils sont extensibles)

{{< pnote >}}

{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
{{< imageg src="editeur-02.png" legende="Capture d'écran de l'éditeur de texte Vim" >}}
{{< psectiono >}}


{{< psectioni >}}
## 5. Les IDE graphiques : Atom ou VSCode/VSCodium

- rappel : les IDE sont des éditeurs de texte avec des fonctionnalités supplémentaires
- il existe beaucoup d'IDE ([voir une liste/comparaison de nombreux IDE sur Wikipédia](https://en.wikipedia.org/wiki/Comparison_of_integrated_development_environments))
- SublimeText, Atom et VSCode sont les plus utilisés actuellement

{{< pnote >}}

{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
{{< imageg src="editeur-03.png" legende="Capture d'écran de l'éditeur de texte VSCodium" >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 5. Les IDE graphiques : Atom ou VSCode/VSCodium
{{< /pcache >}}
### Quelques fonctionnalités communes

- personnalisation de l'interface graphique (paramètres et système de thèmes)
- gestion de version simplifiée _dans_ l'éditeur
- extensions très nombreuses
- code source ouvert (pas pour SublimeText qui est propriétaire)

{{< pnote >}}

{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 5. Les IDE graphiques : Atom ou VSCode/VSCodium
{{< /pcache >}}
Visite guidée de VSCodium !

{{< pnote >}}

{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
## 6. Les éditeurs de texte Markdown

- des éditeurs de texte qui sont pensés pour le format/balisage Markdown
- des fonctions de prévisualisation simplifiée
- des possibilités d'export

{{< pnote >}}

{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 6. Les éditeurs de texte Markdown
{{< /pcache >}}
### Quelques exemples

- [Typora](https://typora.io/) : non libre et payant, interface soignée
- [iA Writer](https://ia.net/writer) : propriétaire et pas disponible pour tous les systèmes d'exploitation. Interface très soignée, modèles de documents, exports possibles
- [Zettlr](https://zettlr.com/) : libre, disponible pour tous les systèmes d'exploitation, **pensé pour l'écriture académique**

{{< pnote >}}

{{< /pnote >}}

{{< psectiono >}}
