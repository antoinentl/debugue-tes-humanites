+++
title = "Séance 03 - Bash ou comment industrialiser le terminal"
date = "2022-01-05"
date_p = "2022-02-11"
description = "Découverte de Bash, création de quelques scripts, détournements créatifs."
layout = "archives-diapositive"
visible = true
saison = "2"
+++
{{< psectioni >}}
1. Qu'est-ce que Bash ?
2. Cas d'usages 1 : créer et convertir des fichiers
3. Cas d'usages 2 : création et exercice
{{< psectiono >}}

{{< psectioni >}}
## 1. Qu'est-ce que Bash ?

- Bash (Bourne-Again shell) est l'_interpréteur en ligne de commande_ d'Unix (Linux, Mac)
- un script Bash ou un script shell est un fichier (en `.sh`) qui comporte une ou plusieurs instructions ou commandes
- ces commandes sont executées les unes à la suite des autres
- c'est un très bon moyen d'automatiser des tâches

{{< pnote >}}
Utiliser Bash en ligne : https://www.onlinegdb.com/online_bash_shell

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Cas d'usages 1 : créer et convertir des fichiers

1. vérifier que vous disposez de pandoc
2. créer un fichier `auto.sh`
3. créer une commande pandoc pour convertir du Markdown vers du HTML
4. dans cette commande, remplacer le nom du fichier par `$1`
5. tester le script

{{< psectiono >}}


{{< psectioni >}}
## 3. Cas d'usages 2 : création et exercice

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Cas d'usages 2 : création et exercice
{{< /pcache >}}
### Atelier de création dans le cours FRA3715 *Écriture et nouveaux médias* (2021)

1. Performativité de l'écriture numérique
2. Fonctionnement interactif d'un récit
3. Écrire avec/selon la machine

{{< pnote >}}
Dans le cadre du cours FRA3715 *Écriture et nouveaux médias* (session Hiver 2021), une séance a été consacrée à « l'écriture par écriture » et a été l'occasion d'un atelier de création avec Bash. 
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Cas d'usages 2 : création et exercice
{{< /pcache >}}
### Programmer un récit en Bash 

Possibles inspirations :

- *Test de Turing*  
- *Temps du récit* 
- *Qui-est-ce* 
- *Syndrome de la page blanche* 

{{< pnote >}}
Les possibles sujets d'inspiration fournis aux étudiant·e·s (qui peuvent aussi servir lors de l'excercice) ont été les suivants : 

- *Test de Turing* : Construire un dialogue avec ma machine  
- *Temps du récit* : Travailler la latence d'apparition du texte : soit comme rythme poétique/suspens/dysfonctionnement
- *Qui-est-ce* : Construire le récit d'une recherche d'information/identité par des requête
- *Syndrome de la page blanche* : Construire le récit d'une écriture qui n'avance pas, qui dysfonctionne 

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Cas d'usages 2 : création et exercice
{{< /pcache >}}
### [Online Bash Shell](https://www.onlinegdb.com/online_bash_shell)

**Attention :** Online Bash Shell n'a pas de sauvegarde automatique sans création de compte

ou 

### Création d'un fichier .sh puis `bash fichier.sh` dans le terminal 

{{< pnote >}}
--> Comme Online Bash Shell ne permet pas la sauvegarde sans création de compte, il faut donc soit téléchager le script avant de fermer la fenêtre Online Bash Shell, soit sauvegarder le script dans un autre document avec l'extension .sh puis le lancer dans le terminal avec la commande : `bash fichier.sh`

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Cas d'usages 2 : création et exercice
{{< /pcache >}}
### Quelques commandes

1. Générer du texte 
2. Jouer avec les commandes de base
3. Créer des rythmes
4. Aligner/Couper/Renverser
5. Texte en interaction

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Cas d'usages 2 : création et exercice
{{< /pcache >}}
### 1. Générer du texte 

 ```
 echo "texte"; 
 ```
ou définir par une valeur :

 ```
 valeur="texte";
 echo ${valeur}
 ```

ou afficher la valeur dans un texte :

 ```
 valeur="texte";
 echo "je suis du ${valeur}"
 ```

{{< pnote >}}
**Attention à ne pas insérer d'espace avant ou après le "=" de la valeur.**
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Cas d'usages 2 : création et exercice
{{< /pcache >}}
### 2. Jouer avec les commandes de bases

Afficher la date : 
 ```
horaire=`date`
echo "Jounal de bord du $horaire"
 ```
 ou 

```
horaire=$(date)
echo "Jounal de bord du $horaire"
```

Afficher la situation : 
 ```
lieu=`pwd`
echo "Je me trouve toujours à bord du $lieu"
 ```

Demander mon identité : (hors interface en ligne)
 ```
identite=`whoami`
echo "Sur mon uniforme, une étiquette qui porte le nom de $identite"
 ```


{{< pnote >}}
Selon les configurations du terminal, d'autres commandes peuvent être impliquées si elles sont installées au préalable (ce qui n'est pas possible sur l'environnement en ligne d'Online Bash Shell). Typiquement la commande `whoami` ne fonctionnera pas sur Online Bash Shell. 
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Cas d'usages 2 : création et exercice
{{< /pcache >}}
### 3. Créer des rythmes

 Latence : 
 ```
sleep 0.9 ;
 ```

 Texte infini : 
  ```
yes "texte";
 ```

 Effacer les lignes précédentes : 
  ```
clear
 ```


{{< pnote >}}
Le temps est exprimé en secondes.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Cas d'usages 2 : création et exercice
{{< /pcache >}}
### 4. Aligner/Couper/Renverser

Aligner : 
 ```
T[0]='texte1'
T[1]='texte2'
echo ${T[0]} ${T[1]}
 ```
 ou 

  ```
 T[0]='texte1'
T[1]='texte2'
  echo ${T[*]}
 ```

Couper : 
  ```
T[0]='texte non coupé'
echo ${T[0]}| cut -c1-5,9-15
 ```

Renverser : 
  ```
 echo "Je perds l'équilibre" | rev
 ```

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Cas d'usages 2 : création et exercice
{{< /pcache >}}
### 5. Texte en interaction 

Interaction simple : 
 ```
echo 'Entrez votre nom :' 
read p 
echo "Bonjour $p !"
 ```

Interaction par choix : 
```
echo "To be or not to be ?"
            select i in être non-être; do
                if [ "$i" = "être" ]; then
                    echo "Sois";
                    break
                elif [ "$i" = "non-être" ]; then
                    echo "Tant pis"
                    break
                else
                    echo "Pas de troisième choix dans ce script"
                fi
        done
```
{{< pnote >}}
Il est également possible de créer des conditionnelles avec le [Case Statement](https://linuxize.com/post/bash-case-statement/?fbclid=IwAR3GcyEn2DCNjv8VRj4UVVp9EXTCGxtDO6qAWrwNI-3zSkhJypVXSoeAzgQ).

Les conditionnelles sont bien entendu emboîtables/encastrables, ce qui peut complexifier le récit. 
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Cas d'usages 2 : création et exercice
{{< /pcache >}}
### Exemple 1 

- [*La machine à poème*](https://demo.hedgedoc.org/rmCNJctpSKWGCNmKQtCdNw#) - Antoine Sweeney


{{< pnote >}}
Cet exemple est un générateur de poèmes personnalisés. 
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Cas d'usages 2 : création et exercice
{{< /pcache >}}
### Exemple 2 

préalable : dans votre terminal
```
sudo apt-get espeak 
```

- [*Faire parler machine*](https://demo.hedgedoc.org/H7THHzQESkSyzzQ8dpvP6A#) - à coller dans un fichier de votre machine 

puis lancer script : 

```
bash nom_fichier.sh
```

{{< pnote >}}
Il faut au préalable avoir installé la commande espeak sur votre terminal. 
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Cas d'usages 2 : création et exercice
{{< /pcache >}}
### Pour plus d'informations sur les commandes bash 

[Memo](https://perso.univ-st-etienne.fr/me63854h/enseignements/L2_SE_16/memo_bash.pdf)

{{< pnote >}}
Bash a une communauté très active : il ne faut donc pas hésiter à faire des recherches sur des fonctionnalités particulières ou des problèmes rencontrés. Les forums sont une bonne source de solutions. 
{{< /pnote >}}

{{< psectiono >}}
