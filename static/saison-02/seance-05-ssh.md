+++
title = "Séance 05 - SSH et les connexions à distance"
date = "2022-01-05"
date_p = "2022-03-11"
description = "Comment rentrer en contact avec d'autres machines, seul·e ou à plusieurs."
layout = "archives-diapositive"
visible = true
saison = "2"
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.1dea32ds/17f6fb59f7c32050e153adaa21d8adb38811f6a2"
+++
{{< psectioni >}}
{{< pcache >}}
1. SSH : pourquoi et comment ?
2. Les clés SSH
3. Se connecter à un serveur : quelques prérequis
4. Connexion à un serveur et découverte de Tmux
{{< /pcache >}}

{{< pnote >}}
Cette séance est une (courte) introduction à SSH, avec quelques éléments théoriques et une approche aussi pratique que possible, afin de découvrir et de maîtriser un moyen pour communiquer et interagir avec un serveur.
N'oublions pas qu'un serveur, c'est avant tout l'ordinateur d'un·e autre : comprendre les enjeux de connexion et d'interaction avec un serveur c'est un peu mieux comprendre comment fonctionne un ordinateur, et peut-être _votre_ ordinateur.

Cette séance de Débugue tes humanités ne délivre que quelques informations concernant SSH, des liens vers des articles, des vidéos ou des documentations permettent d'en savoir plus et d'approfondir ce qui est présenté ici comme un très courte initiation.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. SSH : pourquoi et comment ?
> **Secure Shell (SSH) est à la fois un programme informatique et un protocole de communication sécurisé.** Le protocole de connexion impose un échange de clés de chiffrement en début de connexion. Par la suite, tous les segments TCP sont authentifiés et chiffrés. Il devient donc impossible d'utiliser un sniffer pour voir ce que fait l'utilisateur.  
> [Source : Wikipédia](https://fr.wikipedia.org/wiki/Secure_Shell)

{{< pnote >}}
Ceci étant dit, concrètement SSH permet de communiquer avec une machine distante via Internet (via le port 22), dans la pratique cela correspond aux usages suivants :

- envoi ou récupération d'informations en utilisant SCP (Secure copy protocol) ;
- connexion à un serveur pour travailler _dessus_ ;
- récupération (_fetch_/_pull_) ou envoi (_push_) d'informations dans un dépôt Git ;
- téléchargement ou téléversement de fichiers via des logiciels comme Filezilla avec SFTP (SSH File Transfer Protocol) ;
- etc.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
{{< /pcache >}}

{{< imageg src="ssh-schema.png" >}}

{{< pnote >}}
Voici ce qu'il se passe en détail :

1. l'utilisateur essaye de se connecter avec son client (un ordinateur) à un serveur, par exemple avec la commande `ssh antoine@truc.ecrituresnumeriques.ca`
2. le serveur, qui dispose de la clé publique de l'utilisateur `antoine`, envoie en réponse en réponse cette clé publique
3. la clé publique est liée à une clé privée via un système de chiffrement/cryptage, le client dispose des 2 clés, et peut donc vérifier la correspondance entre les deux
4. si correspondance il y a, la connexion est établie et il est donc possible d'échanger des fichiers de façon sécurisée
5. la suite de ssh `antoine@truc.ecrituresnumeriques.ca` donnera lieu à une connexion sur le serveur `truc.ecrituresnumeriques.ca` via le terminal du client

Ce protocole permet deux choses :

1. authentifier l'utilisateur ou l'utilisatrice qui se connecte à la machine distante
2. sécuriser les échanges : par exemple empêcher qu'une information soit interceptée et soit comprise (les données qui transitent sont chiffrées), voir empêcher qu'une information soit modifiée avant qu'elle n'arrive au destinataire

Pour découvrir l'origine de SSH et son fonctionnement, cette courte vidéo de 9 minutes est bien plus précise que les quelques lignes écrites ici : [How Secure Shell Works (SSH) - Computerphile](https://www.youtube.com/watch?v=ORcvSkgdA58)

Mais rien ne vaut une démonstration !

SSH peut être utilisé avec le duo classique identifiant et mot de passe, mais SSH est souvent utilisé avec un système de clés permettant une plus grande sécurité de connexion et de transmission des informations.
Dans ce cas il faut disposer d'une clé publique et d'une clé privée, l'une étant partageable et l'autre devant resté secrète (c'est-à-dire non partagée et uniquement sur la machine du client).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Les clés SSH
Principe :

- une clé publique est distribuée sur les serveurs ou les machines sur lesquels on souhaite se connecter ;
- une clé privée reste sur le poste du client, et peut être protégée par un mot de passe ;
- la clé publique est **la clé de chiffrement** ;
- la clé privée est **la clé de déchiffrement**.

{{< pnote >}}
Cette méthode s'appelle [le chiffrement asymétrique](https://fr.wikipedia.org/wiki/Cryptographie_asym%C3%A9trique).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Les clés SSH
{{< /pcache >}}
Comment créer/gérer des clés SSH ?

Les étapes :

1. créer une paire de clés : une clé privée et une clé publique
2. indiquer à son ordinateur que ces clés doivent être ajoutées au trousseau
3. distribuer la clé publique aux machines sur lesquelles on souhaite se connecter

{{< pnote >}}
Il s'agit ici des principes, en pratique cela se traduit par un certain nombre de commandes et de programmes permettant de réaliser ces actions.
Voir par exemple cette documentation pour créer une clé SSH et ensuite l'ajouter à la plateforme GitHub : [Generating a new SSH key and adding it to the ssh-agent](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Se connecter à un serveur : quelques prérequis
Lors d'une connexion à un serveur via le programme informatique SSH :

1. l'interface graphique permettant de réaliser cela est un terminal
2. une fois connecté·e sur le serveur, les logiciels et programmes disponibles sont ceux du serveur (et non ceux du client/la machine de l'utilisateur·trice)
3. il faut donc apprendre :
    - à utiliser un terminal
    - à utiliser les outils installer _par défaut_ sur Linux/Debian (le type de système d'exploitation le plus répandu pour les serveurs)

{{< pnote >}}
Si vous avez configuré votre machine pour avoir un environnement adapté à vos pratiques, ce ne sera pas forcément le cas sur le serveur sur lequel vous allez travailler.
Si votre réflexe est de vouloir installer les logiciels auxquels vous êtes habitué ou de modifier la configuration de ceux existants/disponibles, attention :

- d'autres personnes que vous accèdent probablement à ce serveur, peut-être sur la même session, il convient donc de conserver un environnement commun ;
- les configurations ou logiciels pourraient être supprimés, il ne vaut mieux pas tenir dessus.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. Connexion à un serveur et découverte de tmux
L'exercice va consister à se connecter à un serveur et à modifier des fichiers :

Voir pendant la séance.

{{< pnote >}}
Cet exercice ne peut être réalisé que pendant la séance, le compte communiqué pendant la séance étant supprimé quelques heures après.
{{< /pnote >}}
{{< psectiono >}}


