+++
title = "Séance 06 - Sauvegarder ses données"
date = "2022-01-05"
date_p = "2022-03-25"
description = "Pourquoi et comment sauvegarder ses données dans un contexte de recherche académique. Principes, organisation et implémentation technique."
layout = "archives-diapositive"
visible = true
saison = "2"
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.fdd9335t/bbab7f6cb67c557f19ba817a0452828a23d053be"
+++
{{< psectioni >}}
{{< pcache >}}
## Plan de la séance
1. Sauvegarder des données, pourquoi ?
2. Quelques principes pour la sauvegarde de nos données
3. Les types de sauvegarde
4. Rsync : synchronisé vite et bien
5. Chaîne de sauvegarde
{{< /pcache >}}

{{< pnote >}}
Cette séance est une approche (parmi d'autres) aux questions de sauvegarde de données : il ne s'agit en aucun cas d'une formation à la sauvegarde informatique ou à une méthode complète.
Comme souvent dans l'informatique (et dans la vie), il s'agit avant tout de comprendre les enjeux et de prendre conscience des différents paramètres.
L'approche proposée ici a pour objectif de rendre la gestion de la sauvegarde de vos données possible tout en prenant en compte les parties les plus délicates de ce type d'opération.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. Sauvegarder des données : pourquoi ?
> sauvegarder, verbe transit.  
> Conserver, maintenir intact quelque chose.  
> [CNRTL](https://www.cnrtl.fr/definition/sauvegarder)

{{< pnote >}}
La grande question est celle de savoir _pourquoi_ nous avons besoin de sauvegarder des données, cette interrogation peut être divisée en plusieurs points :

- que se passe-t-il si je perds mon ordinateur ?
- que se passe-t-il si ma maison brûle ?
- est-ce que je veux pouvoir accéder à mes données dans 10 ans ?
- est-ce que j'ai besoin de retrouver une ancienne version d'un fichier ?
- qui a besoin d'accéder à mes données et quand ?
- etc.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Quelques principes pour la sauvegarde de nos données

1. plusieurs types de données
2. la fréquence
3. les lieux/plateformes de stockage
4. la vérification

{{< pnote >}}
Avant de se poser la question de quelle solution technique adopter, il faut s'interroger sur quelques principes qui vont avoir une grande influence sur la suite.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Quelques principes
{{< /pcache >}}
### 2.1. Plusieurs types de données

- **données actives** : utilisations/modifications fréquentes
    - projets actifs
		- textes en cours d'écriture
		- annotations
		- bases de données (corpus, données bibliographiques, etc.)
		- etc.
- **données archivées** : accès principalement en lecture
   - livres et documents à consulter (sans annotation)
	 - musique et vidéo
	 - projet ancien/inactif
	 - images
	 - etc.

{{< pnote >}}
Dans le premier cas il s'agit de données qui doivent être accessibles très régulièrement en lecture et en écriture : il ne s'agit pas seulement de pouvoir y accéder mais aussi de devoir les modifier.
Ces données doivent être sauvegarder fréquemment pour pouvoir retrouver les modifications du jour (par exemple).

Dans le second cas il s'agit d'informations qui vont être rarement modifiées, par exemple dans le cas d'un projet passé et terminé.

Une bonne façon de faire la différence entre les deux est de se poser la question suivante : si je perds mon ordinateur et que j'ai besoin d'accéder à un fichier :

- est-ce que cet accès est urgent ?
- est-ce que c'est un problème si je retrouve une version de ce fichier d'il y a 2 mois ?

Une donnée peut passer d'active à archivée et inversement.
Idéalement, l'organisation sur votre ordinateur peut refléter cette distinction.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Quelques principes
{{< /pcache >}}
### 2.2. La fréquence

- moins d'une heure : **synchronisation continue**
- moins d'un jour : **synchronisation régulière**
- moins d'un mois : **synchronisation ponctuelle**

{{< pnote >}}
Ce découpage est quelque peu arbitraire, mais il résume bien une situation que l'on rencontre : si nous travaillons sur un texte, la perte d'une heure d'écriture peut être critique.
En revanche pour d'autres travaux, une sauvegarde quotidienne peut suffire.
Selon le type d'usage ou de données, la fréquence va varier, et les types de sauvegardes également.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Quelques principes
{{< /pcache >}}
### 2.3. Les dispositifs/lieux/plateformes de stockage

{{< imageg src="sauvegarder-dispositifs-lieux.png" legende="Liste des dispositifs possibles de stockage." >}}

{{< pnote >}}
Ce qu'il faut tout d'abord noter ici c'est que le point de départ c'est l'ordinateur, et plus spécifiquement votre station de travail principale (dans le cas où vous devez travailler sur plusieurs machines) : c'est là où sont produites (ou récupérer) les données.
Les flèches représentent la sauvegarde des données : cette sauvegarde est unidirectionnelle, elle part de l'ordinateur pour aller vers un disque dur ou une plateforme de stockage.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Quelques principes
### 2.3. Les dispositifs/lieux/plateformes de stockage
{{< /pcache >}}

- votre ordinateur (portable ou non) : par défaut le dispositif qui produit et contient les données
- disque dur : avec vous, chez vous, chez quelqu'un d'autre
- des lieux physiques : votre _maison_, d'autres lieux
- plateforme _en ligne_ : l'ordinateur de quelqu'un d'autre accessible en ligne

{{< pnote >}}
Cette distinction permet de 
Si vous avez un _chez vous_ (une maison, un appartement, un lieu de vie abrité dans lequel vous pouvez vous rendre régulièrement et dans lequel vous pouvez conserver un disque dur sans crainte), alors vous pouvez y stocker votre ordinateur et un disque dur.
Ce point est important : il faut déterminer des espaces physiques que vous connaissez pour pouvoir y stocker un ou plusieurs disques durs.

J'écarte ici les dispositifs du type tablette ou téléphone intelligent, ils ne constituent pas des dispositifs de stockage et de sauvegarde, trop instables et peut-être trop mobiles.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Quelques principes pour la sauvegarde de nos données
{{< /pcache >}}
### 2.4. La vérification
La partie la plus délicate :

- est-ce que les données ont toutes été copiées ?
- est-ce que le support de stockage fonctionne bien ?
- plusieurs solutions : vérifications par échantillonnage notamment.

{{< pnote >}}
La sauvegarde ne suffit pas, car si les données sont stockées sur plusieurs dispositifs, encore faut-il que ces données soient lisibles et utilisables.
Sans aller jusqu'à l'émulation, il s'agit de s'assurer que les données aient bien été copiées et soient encore lisibles.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Les types de sauvegarde

- sauvegarde : copie conforme (copié/collé)
- synchronisation : copie si modification
- fréquence : continue/régulière/ponctuelle
- versionnement : conservation d'un historique

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. rsync : synchronisé vite et bien
> rsync remote synchronization, (en français : « synchronisation distante ») est un logiciel libre de synchronisation de fichiers, distribué sous licence GNU GPL. La synchronisation est unidirectionnelle, c'est-à-dire qu'elle copie les fichiers de la source en direction de la destination. rsync est donc utilisé pour réaliser des sauvegardes incrémentielles ou différentielles ou pour diffuser le contenu d'un répertoire de référence.

{{< pnote >}}
Pourquoi utiliser rsync ?
Parce que c'est un moyen simple et très efficace de sauvegarder et mettre à jour ses données.
Et notamment en gardant la trace : des dates de publication précises (conservées au moment de la copie) et des droits d'utilisation (très utile).
Par ailleurs le fonctionnement de rsync est tel qu'une sauvegarde d'un ordinateur ne prend que quelques minutes (à partir de la deuxième sauvegarde) :

- rsync est capable de comparer les fichiers d'origine et ceux conservés sur le deuxième support/dispositif ;
- si aucune modification n'a été faite sur un fichier, rsync ne le copie/colle pas ;
- ainsi un fichier lourd ne sera pas copié à chaque sauvegarde.

Pour donner un exemple : la sauvegarde complète de mon ordinateur (400Go) avec rsync sur un disque dur prend environ 15 minutes.

Le logiciel (avec interface graphique) peut vous faciliter l'utilisation de rsync (d'habitude utilisé en ligne de commande) : [https://www.opbyte.it/grsync/](https://www.opbyte.it/grsync/), a priori le logiciel est également disponible sous Mac et Windows.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 5. Chaîne de sauvegarde

{{< imageg src="sauvegarder-chaine.png" >}}

{{< pnote >}}

1. fichiers versionnés (typiquement avec Git) et sauvegardés sur un serveur distant (comme GitHub, l'instance GitLab d'Huma-Num, etc.). À chaque versionnement je peux envoyer mes données sur un autre serveur, qui va conserver les fichiers et l'historique des modifications. Pour les fichiers très lourds, pensez à Git LFS ;
2. données actives synchronisées en temps réel : dès que je fais une modification un logiciel synchronise mes fichiers (Dropbox, Nextcloud, etc.) ;
3. données actives sauvegardées régulièrement avec rsync et éventuellement un script qui fait la sauvegarde à un moment précis ;
4. même chose pour les données archivées ;
5. données actives sauvegardées sur un second support ;
6. idem pour les données archivées.

Pour résumer voici la liste des actions nécessaires :

- envoi des fichiers versionnés lorsque je travaille dans un dépôt Git ;
- synchronisation des données actives sans intervention (vérifier tous les mois que les données sont lisibles via un accès en ligne) ;
- une fois par semaine/mois : sauvegarde sur un premier disque dur avec rsync ;
- une fois par mois : sauvegarde sur un second disque dur avec rsync également.

{{< /pnote >}}
{{< psectiono >}}

