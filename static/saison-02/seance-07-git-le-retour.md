+++
title = "Séance 07 - Git le retour"
date = "2022-01-05"
date_p = "2022-04-08"
description = "Usage réel, paramètres et configuration, gestion des conflits, merge requests, etc."
layout = "archives-diapositive"
visible = true
saison = "2"
+++
{{< psectioni >}}
{{< pcache >}}
## Plan de la séance

1. Les principes de Git
2. Configurer Git
3. Retour sur les commandes usuelles
4. Conflits, forks et rebase
{{< /pcache >}}

{{< pnote >}}
Revenons sur Git après [une séance de découverte lors de la première saison](/seance-11-versionner/).
Pourquoi s'évertuer à utiliser absolument Git ?
Réponse en quelques principes théoriques et avec des manipulations !
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. Les principes de Git

- suivre un projet
- développements/évolutions non linéaires (branches)
- historique général et particulier
- système de gestion de versions distribué
- complexité relative
- logiciels/plateformes qui simplifient l'usage

{{< pnote >}}
Git est pensé pour versionner **un projet**, seul·e ou à plusieurs.
Si Git permet de versionner des fichiers, il ne faut pas oublier que l'objectif final est bien de suivre un projet dans son ensemble et pas forcément un fichier en particulier
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Les principes de Git
{{< /pcache >}}
### Quelques définitions

- **dépôt** : ensemble des fichiers versionnés
- **commit** : enregistrement des changements dans un dépôt
- **fork** : divergence d'un projet, une copie qui va vivre sa propre vie (ou presque)
- **cloner** : copier un projet chez soi pour pouvoir y contribuer
- **conflit** : lorsque l'on tente de réunir voir de _fusionner_ plusieurs versions d'un même projet

{{< pnote >}}
Un commit est une série d'informations :

- un identifiant
- une date
- un message
- une liste de modifications associées : les fichiers modifiés

Pour comprendre comment fonctionne Git il faut comprendre ce qu'est un _commit_ : ce n'est pas un enregistrement classique, mais l'état du projet tout entier après une série de modifications sur un ou plusieurs fichiers.

Si le _fork_ n'est pas toujours habituel (c'est la création d'une copie d'un projet qui pourra être indépendant de la version d'origine), le fait de _cloner_ un dépôt est beaucoup plus fréquent.
La différence entre les deux est aussi une différence de pratique :

- en _forkant_ on s'approprie le projet, si besoin on pourra soumettre des modifications à la _version originale_ tout en travaillant sur _notre version_ ;
- en _clonant_ le projet je conserve le projet original, mais je risque de perturber les développements.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Les principes de Git
{{< /pcache >}}
### À retenir

- bien configurer Git
- Git ≠ GitHub/GitLab
- la bible : [https://git-scm.com/book/fr/v2](https://git-scm.com/book/fr/v2)
- il est presque impossible de supprimer quelque chose

{{< pnote >}}
Comme beaucoup d'outils, Git nécessite quelques réglages préalables !
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Configurer Git

- configuration générale : fonctionnement de Git pour tous les projets
- configuration particulière : pour un projet
- clés SSH ?…

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Configurer Git
{{< /pcache >}}
### 2.1. Configuration générale

- basique : un nom et une adresse
- avancée : éditeur par défaut, branche par défaut
- voir la configuration : `git config --list`
- modifier la configuration, exemple : `git config --global user.name "Antoine Fauchié"`

{{< pnote >}}
Git repose sur la reconnaissance des personnes qui contribuent ensemble à un même projet, il est donc primordial de bien identifier qui est qui.
Pour cela il faut au moins une adresse électronique, mais avec un nom ou un pseudonyme on gagne en lisibilité.

Il est aussi possible de spécifier [beaucoup d'autres paramètres](https://git-scm.com/docs/git-config), comme l'éditeur de texte par défaut qui sera utilisé pour les messages des _merges_ par exemple.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Configurer Git
{{< /pcache >}}
### 2.2. Configuration particulière

- par défaut : configuration générale qui s'applique
- a minima : les branches disponibles en local et le dépôt distant
- possible de spécifier

{{< pnote >}}
Il est par exemple possible de spécifier un utilisateur différent pour chaque dépôt/projet.
Cela peut être pratique pour distinguer des usages professionnels et des usages plus personnels.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Configurer Git
{{< /pcache >}}
### 2.3. Clés SSH ?…


{{< psectiono >}}


{{< psectioni >}}
## 3. Retour sur les commandes usuelles

- `git init` : initialiser un dossier
- `git status` : voir l'état du projet
- `git log` : afficher l'historique de la branche actuelle
- `git add` : ajouter un fichier dans l'index avant de commiter
- `git commit` : déclarer des modifications
- `git branch` : créer une nouvelle branche
- `git checkout` : pour basculer sur une branche
- `git push` : envoyer les modifications sur un dépôt distant
- `git fetch` : récupérer les modifications d'un dépôt distant
- `git pull` : récupérer les modifications d'un dépôt distant **et** les fusionner avec le dépôt local

{{< pnote >}}
Pour afficher un historique plus détaillé : `git log --decorate=full --raw`
Ou pour afficher l'arbre des branches : `git log --branches --remotes --tags --graph --oneline --decorate --pretty=format:"%h - %ar - %s"`

Le concept de branche est un raccourci bien pratique : cela permet d'identifier des commits facilement, et de créer un 
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}

{{< imageg src="basic-branching-6.png" >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Retour sur les commandes usuelles
{{< /pcache >}}
### Exercice 1

1. créer un dossier : `mkdir bac-a-sable`
2. aller dans ce dossier : `cd bac-a-sable`
3. créer un fichier : `echo "Mon texte" >fichier-01.txt`
4. ajouter le fichier dans l'index et commiter
5. afficher l'historique

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. Conflits, forks et rebase

- un conflit : il faut choisir
- forks : comment maintenir sa version à jour ?
- rebase : les bonnes pratiques difficiles à mettre en place
{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Conflits, forks et rebase
{{< /pcache >}}
### Un conflit : choisir/arbitrer

- pour distinguer les portions : des signes typographiques sont utilisés
- certains éditeurs de texte facilitent la visualisation des conflits
- si le conflit n'est pas résolu : impossible de continuer

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Conflits, forks et rebase
{{< /pcache >}}
### Exercice 2 : gestion d'un conflit
Suite à l'exercice 1, nous allons créer plusieurs modifications :

1. créer une nouvelle branche appelée `modifs` : `git checkout -b modifs`
2. vous êtes désormais sur cette nouvelle branche
3. modifier le fichier, par exemple : `echo "Autre texte hop là" >fichier-01.txt`
4. enregistrer vos modifications dans Git : `git commit -a -m "révision de la première ligne"`
5. retourner sur la branche principale : `git checkout master` (ou `git checkout main` selon votre configuration)
6. effectuer une nouvelle modification : `echo "Ceci est mon texte" >fichier-01.txt`
7. enregistrer vos modifications : `git commit -a -m "réécriture"`
8. tenter de fusionner les deux branches : `git merge modifs`

Vous devez avoir un conflit !

{{< pnote >}}
Vous devez ouvrir le fichier pour résoudre le conflit :

- dans un éditeur de texte comme Atom ou VSCode, des options vont vous être proposées : quelle version vous souhaitez choisir ;
- dans des éditeurs de texte comme Vim, Nano ou autre, vous devez supprimer ce que vous ne voulez pas retenir :
    - les `<<<<<<`, les `======` et les `>>>>>>`, ainsi que les mentions des commits et de `HEAD`
		- c'est à vous de conserver manuellement ce qui vous intéresse
- une fois ces modifications faites, vous devez enregistrer le fichier, et commiter tout cela ;
- effectuer un `git status` pour s'assurer que tout est en ordre !

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Conflits, forks et rebase
{{< /pcache >}}
### forks : comment maintenir sa version à jour ?

1. créer un fork du projet sur votre GitLab Huma-Num
2. cloner le dépôt sur votre ordinateur (dans un autre dossier que le projet précédent)
3. ajouter l'indication du fork en local : `git remote add upstream git@gitlab.huma-num.fr:antoinefauchie/bac-a-sable-2.git`
4. pour récupérer les modifications du dépôt d'origine : `git fetch upstream`
5 pour synchroniser ces modifications avec _votre_ dépôt : `git rebase upstream/master`

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Conflits, forks et rebase
{{< /pcache >}}
### rebase : les bonnes pratiques difficiles à mettre en place
Un tutoriel complexe à explorer si vous êtes motivé·e :

[https://git-rebase.io](https://git-rebase.io)

{{< pnote >}}
Mais si nous avons le temps nous allons regarder un exemple.
{{< /pnote >}}
{{< psectiono >}}
