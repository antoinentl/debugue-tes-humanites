+++
title = "Séance 08 - Fabriquer des pages web"
date = "2022-01-05"
date_p = "2022-04-22"
description = "Introduction à HTML et CSS, conversions à partir de Markdown avec Pandoc."
layout = "archives-diapositive"
visible = true
saison = "2"
+++
{{< psectioni >}}
{{< pcache >}}
## Plan de la séance

1. HTML : baliser le texte
2. CSS : mettre en forme (mais pas que)
3. Écrire du HTML sans écrire du HTML
4. Les templates Pandoc
{{< /pcache >}}

{{< pnote >}}
Cette séance est dédiée à la découverte du langage HTML.
Attention, il s'agit d'une courte introduction qui permet de comprendre comment créer une page HTML, et non d'une formation exhaustive sur la question qui mériterait plusieurs jours.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. HTML : baliser le texte

- pourquoi des balises ?
- quelle version de HTML ?
- un format pérenne ?

{{< pnote >}}
Les langages de balisage sont anciens, HTML dérive du format SGML et permet de **structurer** l'information pour qu'elle puisse être interprétée par un navigateur web.
C'est un langage de balisage _non léger_, contrairement à Markdown qui lui est léger, car les balises sont nombreuses et ne permettent pas une lecture fluide par les humains.

Il y a eu plusieurs version successives de HTML, et notamment des versions XHTML qui héritaient des propriétés XML.
La version _actuelle_ de HTML est HTML5, un standard très permissif qui accepte les erreurs.

À noter qu'un document HTML doit toujours avoir un `DOCTYPE` permettant d'indiquer le format utilisé, exemple avec une page HTML au format HTML5 :
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
```html
<!doctype html>
<html lang="fr">
  <head>
	  <title>Ma page</title>
	</head>
	<body>
	  <p>Du texte.</p>
	</body>
</html>
```

{{< pnote >}}
Il y a quelques règles à respecter pour écrire en HTML :

- les balises sont déterminées pas le standard, il n'est pas possible de personnaliser les balises ;
- les balises sont imbriquées, elles ne doivent se chevaucher, exemple de ce qu'il ne faut pas faire : `<p><em>du texte</p></em>`.

Un document HTML peut contenir un entête, c'est-à-dire une partie qui décrit la page et qui peut par exemple permettre d'indiquer un certain nombre de métadonnées.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. HTML : baliser le texte
{{< /pcache >}}
### Exercice

- ouvrez un éditeur de texte (VSCode, Codium, Atom, Vim, etc.)
- créez un fichier ma-page.html
- renseignez un titre avec une balise `<h1>`
- ajoutez quelques renseignements sur cette page dans une balise `<details>`
- écrivez quelques lignes avec la balise `<p>`
- enregistrez votre fichier puis ouvrez-le avec votre navigateur favori

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. CSS : mettre en forme (mais pas que)

- appliquer une mise en forme à partir d'une sémantique
- des règles imbriquées
- un langage de programmation

{{< pnote >}}
Le langage CSS est aussi ancien que HTML, CSS signifie _Cascading Style Sheets_ ou feuilles de style en cascade.
L'objectif est de pouvoir attribuer une mise en forme à des éléments HTML.
Ces deux langages sont donc pensés pour aller ensemble.
Il s'agit de la séparation structure et mise en forme, fond et affichage graphique, même si dans les faits CSS permet de faire bien d'autres choses.

En terme de principe, le développement de CSS est très intéressant : ce langage est développé par niveaux, et chaque nouveau niveau doit intégrer le précédent.
Ainsi, chaque nouvelle version doit prendre en compte la précédente, c'est ce qu'on appelle la rétrocompatibilité.

Si il n'y a aucun doute que HTML est un langage de programmation (si vous doutez vous pouvez découvrir [cette vidéo](https://briefs.video/videos/is-html-a-programming-language/), 

Concrètement un fichier CSS ressemble à cela :

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
```
p {
  text-align: right;
}
```

{{< pnote >}}
Chaque élément est décrit selon des propriétés définies dans le standard CSS.
Il est aussi possible de créer des attributs (et leur valeur) _dans_ le langage HTML, de spécifier des propriétés CSS sur ces attributs.

La feuille style peut soit être intégrée _dans_ le fichier HTML à l'intérieur d'une balise `<style>p { text-align: right;}</style>` ou via un fichier appelé ainsi :
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
```
<!doctype html>
<html lang="fr">
  <head>
	  <link rel="stylesheet" href="styles.css">
	  <title>Ma page</title>
	</head>
	<body>
	<p>Du texte.</p>
</html>
```

{{< pnote >}}
Ici le fichier `styles.css` doit être placé dans le même dossier que le fichier HTML.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. CSS : mettre en forme (mais pas que)
{{< /pcache >}}

### Exercice

1. créez un fichier `styles.css` avec votre éditeur de texte que vous placez au même niveau que votre fichier HTML précédemment créé
2. indiquez que la couleur de votre titre de niveau 1 doit être rouge avec la propriété `color`
3. enregistrez votre fichier
4. modifier votre fichier HTML précédent pour que la feuille CSS puisse être appelée
5. affichez votre fichier HTML dans votre navigateur web préféré

{{< psectiono >}}


{{< psectioni >}}
## 3. Écrire du HTML sans écrire du HTML

- écrire/maintenir du HTML : la fausse bonne idée
- pour des documents simples : Markdown (+ YAML)
- Pandoc et ses templates

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. Les templates Pandoc

- le principe des templates
- le système clé/valeur
- des templates dans des templates

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Les templates Pandoc
{{< /pcache >}}

### Utiliser un template Pandoc

- afficher le template par défaut : `pandoc -D html`
- les variables importantes :
  - `$title$`
	- `$body$`
- pour appeler un template avec Pandoc :
`pandoc -f markdown -t HTML --template=mon-template.html mon-fichier.md -o mon-fichier-resultat.html`

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Les templates Pandoc
{{< /pcache >}}

### Exercice

1. créez un fichier `mon-template.html` avec votre éditeur de texte
2. reprenez la structure de votre fichier HTML précédent
3. créez un fichier Markdown avec votre texte, pensez à ajouter un entête avec le titre de votre document
4. indiquez les variables utiles dans votre _template_
5. générez votre fichier HTML avec la commande précédemment présentée
6. affichez cette page dans votre navigateur préféré

La commande Pandoc :  
`pandoc -f markdown -t HTML --template=mon-template.html mon-fichier.md -o mon-fichier-resultat.html`

{{< psectiono >}}

