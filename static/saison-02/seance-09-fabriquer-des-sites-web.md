+++
title = "Séance 09 - Fabriquer des sites web"
date = "2022-01-05"
date_p = "2022-05-06"
description = "Introduction aux générateurs de site statique, création d'un mini site web depuis un modèle."
layout = "archives-diapositive"
visible = true
saison = "2"
+++
{{< psectioni >}}
{{< pcache >}}
## Plan de la séance

1. Qu'est-ce qu'un site web ?
2. Mille façons de fabriquer un site web
3. Approche SSG
4. Créer un site web avec Hugo
{{< /pcache >}}

{{< pnote >}}
Après avoir découvert quelques éléments du langage HTML afin de créer une page web, nous allons approfondir la question de l'automatisation avec cette fois la création d'un site web.
Loin d'avoir épuisé toutes les possibilités quand à la création d'une page web, l'idée est de s'intéresser cette fois à l'organisation de plusieurs éléments avec des outils plus complexes que Pandoc.

L'objectif est triple :

- comprendre les enjeux de la production d'un site web ;
- appréhender les différentes façons de _faire_ un site web de façon plus ou moins automatisée ;
- découvrir l'approche des générateurs de site statique.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. Qu'est-ce qu'un site web ?

- ensemble de pages web organisées
- document complexe comprenant des éléments de navigation
- objet éditorial figé ou en constante évolution

{{< pnote >}}
La grande différence avec une page web réside dans le fait qu'un site web est bien plus compliqué à organiser : les informations proviennent de plusieurs documents source et doivent être agencées en suivant des règles souvent complexes.
Il est question d'arborescence, de hiérarchie, de niveaux, de catégories ou de mots-clés afin de trouver le meilleur moyen (et plus compréhensible) d'organiser des informations.

Attention, cette organisation n'est pas toujours visible :

- un site web facile à utiliser peut _cacher_ une construction très complexe ;
- un site web difficile à manier est souvent la preuve d'un manque de réflexion éditoriale et non le fait de mauvais choix techniques.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Qu'est-ce qu'un site web ?
{{< /pcache >}}
### Exercice d'analyse

- choisissez un site web que vous avez l'habitude d'utiliser, que vous appréciez, et qui comporte plus de 10 pages différentes
- repérez au moins 3 façons de naviguer dans ce site web
- identifiez une piste d'amélioration du site web (structuration, organisation, navigation, mise en forme)

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}



{{< psectioni >}}
## 2. Mille façons de fabriquer un site web

- à la main : facilité de création de _pages_, difficulté de mise à jour des index, légèreté
- CMS : puissance d'organisation, lourdeur de la solution technique, complexité de la migration
- générateur de site statique : faussement nouveau, séparation nette entre contenus et _fabrique_, complexité de prise en main

{{< pnote >}}
Nous avons déjà aperçu la facilité de créer une page web _à la main_, sans convertisseur ou générateur.
Cela fonctionne bien pour une page web, à condition d'accepter la verbosité de HTML.
Pour créer un site web composé de plusieurs pages, l'approche manuelle est assez complexe pour plusieurs raisons :

- si les éléments de navigation changent, alors il faut modifier _toutes_ les pages qui affichaient le dit-menu ;
- un site web comporte souvent des pages d'index qui permettent de répertorier les pages afin de les rendre accessibles, maintenir ces index peut être particulièrement fastidieux.

Si vous souhaitez créer un _petit_ site web fait de quelques pages, un conseil : faite le à la main !

Les CMS, pour Content Management System, ont été pensés pour faciliter la gestion d'objet éditoriaux complexes, et donc des sites comportant plusieurs pages.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. L'approche des générateurs de site statique

- deux fonctions principales :
  - convertir d'un format de balisage à un autre, typiquement de Markdown à HTML
	- organiser les pages à partir d'instructions écrites dans un langage de sérialisation de données
- changement de paradigme : aucun langage dynamique pour _servir_ les pages web
- résultat : un site statique (notion à …)

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. L'approche des générateurs de site statique
{{< /pcache >}}
### Exercice

- rendez-vous sur [https://gitlab.huma-num.fr/ecrinum/demarreur](https://gitlab.huma-num.fr/ecrinum/demarreur)
- explorez les différents dossiers et sous-dossiers afin de comprendre l'architecture du projet :
  - où sont les contenus ?
  - quels sont les fichiers contenant les modèles (ou _templates_) de données ?
  - y a-t-il un fichier de configuration principal ?
- visitez [https://ecrinum.gitpages.huma-num.fr/demarreur](https://ecrinum.gitpages.huma-num.fr/demarreur) afin d'essayer de comprendre comment les pages sont construites selon les informations affichées (page d'accueil, à propos et introduction)

{{< psectiono >}}


{{< psectioni >}}
## 4. Créer un site web avec Hugo

- Hugo : un générateur de site statique parmi d'autres
- avantages : dépendances limitées (un fichier binaire suffit pour l'installation), rapidité, puissance
- inconvénients : courbe d'apprentissage un peu raide, documentation aride, extensibilité complexe
- visite de Hugo avec le mini projet [démarreur](https://ecrinum.gitpages.huma-num.fr/demarreur)

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Créer un site web avec Hugo
{{< /pcache >}}
### Exercice 1

- installez Hugo :
  - Ubuntu/Debian : [téléchargez le fichier `hugo_extended_0.XX.0_Linux-64bit.deb` le plus récent](https://github.com/gohugoio/hugo/releases) puis `sudo dpkg -i [le-fichier].deb`
  - Mac : `brew install hugo`
  - Windows : [voir l'installation sur la documentation](https://gohugo.io/getting-started/installing/#windows)
- clonez le projet sur votre machine : `git clone git@gitlab.huma-num.fr:ecrinum/demarreur.git`
- allez dans le dossier créé : `cd demarreur`
- lancez Hugo pour _voir_ le site en local : `hugo serve`

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Créer un site web avec Hugo
{{< /pcache >}}
### Exercice 2

- modifiez le titre du site, pour cela vous devez trouver le fichier de configuration principal
- ajoutez un "post" : regardez comment sont constitués les deux existants et essayez de comprendre la règle de classement sur la page d'accueil

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 4. Créer un site web avec Hugo
{{< /pcache >}}
### Exercice 1

- installez Hugo :
  - Ubuntu/Debian : [téléchargez le fichier `.deb` le plus récent]() puis `sudo dpkg -i [le-fichier].deb`
  - Mac : `brew install hugo`
  - Windows : [voir l'installation sur la documentation](https://gohugo.io/getting-started/installing/#windows)
- clonez le projet sur votre machine : `git clone git@gitlab.huma-num.fr:ecrinum/demarreur.git`
- allez dans le dossier créé : `cd demarreur`
- lancez Hugo pour _voir_ le site en local : `hugo serve`

{{< psectiono >}}
