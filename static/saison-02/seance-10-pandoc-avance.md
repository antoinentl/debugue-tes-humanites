+++
title = "Séance 10 - Pandoc en mode avancé"
date = "2022-01-05"
date_p = "2022-05-13"
description = "Rappels des usages possibles de Pandoc, découverte des filtres LUA."
layout = "archives-diapositive"
visible = false
saison = "0"
+++
