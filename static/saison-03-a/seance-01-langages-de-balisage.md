+++

title = "Séance 01 - Les langages de balisage"

date = "2021-09-10"

date_p = "2023-01-31"

description = "De Markdown à HTML et XML. Le balisage sémantique."

layout = "diapositive"

visible = true

saison = "3a"

+++

  

{{< psectioni >}}

## Bienvenue !

  

1. Tour d'écrans
2. Qu'est-ce que un langage de balisage ?
3. Les formats de balisage sémantique
4. Lire, éditer et convertir
5. Créer une page web, du markdown vers HTML  

{{< pnote >}}

Pour cette première séance nous vous proposons de découvrir les enjeux de cette formation ainsi que les différentes séances au programme.

{{< /pnote >}}

  

{{< psectiono >}}

  

{{< psectioni >}}

## 1. Tour d'écrans en 3 questions

  

1. votre nom
2. ce que vous faites cette année
3. le projet ou l'initiative _numérique_ qui vous intéresse en ce moment
  

{{< psectiono >}}

  
  

{{< psectioni >}}
## 2. Qu'est-ce que un langage de balisage ?

{{< psectiono >}}

  
  

{{< psectioni >}}

### Le traitement numérique du texte

  
Avec l'utilisation des logiciels de bureautique, on observe une confusion entre structure et mise en forme

Deux paradigmes pour les éditeurs de texte:
- WYSIWYG What You See Is What You Get
- WYSIWYM What You See Is What You Mean


La structure sémantique permet de :
- se repérer dans le texte
- automatiser le traitement

  

{{< psectiono >}}

  

<!--

Faire un exercice avec le début d'un article SP

- ouvrir le texte brut dans hedgedoc

- consulter l'article en ligne

Lien vers l'article: http://www.sens-public.org/articles/1600/

  

utiliser la vue WYSIWYG et WYSIWYM pour baliser au fur et à mesure

  

-->

  

{{< psectioni >}}

  

### Exercice


Baliser le texte d'un article avec du markdown

Lien vers l'article : [ici](http://www.sens-public.org/articles/1600/)

Lien vers le texte brut à baliser : [ici](https://demo.hedgedoc.org/U_Nek6pqSBK32yWMIfFcYg)

{{< pnote >}}
- identifier le titre, sous-titre
- référence (plus compliqué)
- titre en italique
- citation

{{< /pnote >}}

  

{{< psectiono >}}

  
  

{{< psectioni >}}
## 3. Les formats de balisage sémantique
- Markdown
- HTML
- XML
{{< psectiono >}}

  

{{< psectioni >}}

  

### Markdown

- créé en 2004
- langage léger
- lisible par les humains

  
  

{{< psectiono >}}

  
  

{{< psectioni >}}

  

### Les balises de Markdown

- niveaux de titre, paragraphe, citation longue, listes
- texte en gras, soulignée, italique
- lien, image, tableau
- commentaire

  

{{< psectiono >}}

  
  

{{< psectioni >}}

  

### HTML

- Hypertext Markup Language
- créé en 1993
- conçu pour les liens entre les documents (*hyperlinks*)
- cinquième version (en 2008), s'adapte à l'évolution du Web


[Tableau d'équivalence avec Markdown](https://en.wikipedia.org/wiki/Markdown#Examples)

  
  
  

{{< psectiono >}}

  
  

{{< psectioni >}}

  

### Les balises HTML

  
  

```
<html>
    <head>
        <meta charset="utf-8">
        <title>Titre dans l'onglet</title>
    </head>
    <body>
        <h1>Titre</h1>
        <p>Ceci contient un paragraphe.</p>
    </body>
<html>

```
  

Pour consulter le code source (HTML) de l'article Sens Public : `view-source:` + https://www.sens-public.org/articles/1600/ dans le navigateur

  

{{< psectiono >}}

  
  

{{< psectioni >}}

  

### XML

- Extensible Markup Language
- crée en 1998
- conçu pour la structuration stricte de données
- l+es informations sont validées par des schémas
  
Démo sur Oxygen, un éditeur de texte pour XML

{{< psectiono >}}

  
  

{{< psectioni >}}

## 4. Lire, éditer et convertir

{{< psectiono >}}

  

{{< psectioni >}}

  

### Les lecteurs de textes balisés


Tous ces formats sont des fichiers `.txt` "augmentés" par des balises


Un navigateur est capable de lire tous ces formats:
- md
- html
- xml

Avec toujours la possibilité d'afficher uniquement le texte brut (code source)

  

{{< psectiono >}}

  
  

{{< psectioni >}}

  

### Les éditeurs

  

1. éditeurs de fichiers txt : Notepad ou textEdit
2. éditeurs de code : Atom, VS Code ou VS Codium
    - coloration syntaxique
    - terminal, git
3. éditeurs de markdown : Zettlr, Typora, HedgeDoc
    - fonctionnalités plus avancées et interface ergonomique pour l'écriture
    - export vers d'autres formats
4. éditeurs XML
    - permet la validation des schéma
    - autocomplétion et vue auteur
  

{{< psectiono >}}

  

{{< psectioni >}}

  

### La conversion entre langages de balisage


Niveaux de langage : MD < HTML < XML
- de gauche à droite : on maintient l'information et il faut généralement ajouter des balises
- de la droite vers la gauche : il est probable de perdre de l'information


Des logiciels permettent de convertir automatiquement les contenus, comme Pandoc par exemple. Certains sont intégrés dans les éditeurs.

  

{{< psectiono >}}

  

{{< psectioni >}}

  

## 5. Créer une page Web, du markdown vers HTML

  
  

Installation :
- [VS code](https://code.visualstudio.com/download) ou [VS codium](https://github.com/VSCodium/vscodium/releases)
- extension Live Server pour avoir un serveur local

  

{{< psectiono >}}

  

{{< psectioni >}}

  

### Atelier

   

1. Écrire un contenu en .md sur [HedgeDoc](https://demo.hedgedoc.org/)
2. L'exporter en HTML
3. L'ouvrir et l'éditer dans VS Code
4. Lancer LiveServer pour une prévisualisation instantanée

  

{{< psectiono >}}