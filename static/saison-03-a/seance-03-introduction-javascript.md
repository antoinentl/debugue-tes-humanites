+++
title = "Séance 03 - Introduction à Javascript"
date = "2021-09-10"
date_p = "2023-04-11"
description = "Pour développer des interactions sur le Web."
layout = "diapositive"
visible = true
saison = "3a"
+++

{{< psectioni >}}

## Plan du cours

1. JavaScript -- Historique
2. Que peut-on faire avec Javascript ?
3. Lier un script JS à une page HTML
4. Introduction à la syntaxe et à la sémantique JS
5. Atelier JavaScript -- construire un petit jeu

{{< psectiono >}}

{{< psectioni >}}

## JavaScript -- Un langage en constante évolution
- 1993 : Mosaic
- 1994 : Netscape (première _browser war_ -- 1995-2001)
- 1995 : nécessité d'un langage de script côté client pour le Web -- Java vs. Scheme
- [Brendan Eich](https://en.wikipedia.org/wiki/Brendan_Eich)
- 1996 : _JScript_ par Microsoft

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## JavaScript -- Un langage en constante évolution
{{< /pcache >}}
- 1997 : Netscape vers la standardization avec ECMA International (European Computer Manufacturers Association)
- 1998 : _JScript_ est le standard _de facto_
- 2022 : report [_Octoverse_](https://octoverse.github.com/2022/top-programming-languages) réalisé par GitHub -- « JavaScript continues to reign supreme »
- Frameworks : ex. Angular, React, Vue, Svelte -- des boîtes à outils pour le développement Web avec des décisions déjà faites et réfléchies
- [ECMAScript Editions](https://www.w3schools.com/js/js_versions.asp)
{{< psectiono >}}


{{< psectioni >}}
## Que peut-on faire avec JS ?
Manipulation des pages Web, Interactions avec l'utilisateur et le serveur Web.

{{< pnote >}}
Par exemple, in-browser JavaScript est capable de : 
- Ajouter du nouveau HTML à la page, changer le contenu existant, modifier les styles
- Réagir aux actions de l'utilisateur, s'exécuter sur les clics de souris, les mouvements du pointeur, les pressions sur les touches
- Envoyer des requêtes sur le réseau à des serveurs distants, télécharger et téléverser des fichiers, charger le contenu d'une page web sans rafraîchir la page <!--(technologies dites AJAX et COMET)-->
- Obtenir et définir des cookies, poser des questions au visiteur, afficher des messages
- Mémoriser les données du côté client (_Local Storage_)
{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
## Lier une page HTML à un script JS

- `<script src="/path/to/script.js">`
- `<script src="https://link.js">`
- style _embedded_ dans l'HTML avec le tag `<script>`
Plusieurs scripts peuvent être liés à un seul document HTML

{{< psectiono >}}



{{< psectioni >}}

## Premiers exemples
- `alert('hello');`
- `console.log('hello');`
- `document.body.innerHTML = 'hello';`

{{< psectiono >}}



{{< psectioni >}}
## Types de données
- Variables : `var message = 'Hello'` ou `let` ou `const`
{{< pnote >}}
  - `var` : 
  - `let` : permet de déclarer des variables qui sont inhérentes à la portée d'un bloc d'instructions.
  - `const` : permet d'initialiser une valeur que nous ne devrions plus changer.

### Var VS. Let : 

```js
function exampleVar() {
  var x = 10;
  if (true) {
    var x = 20;
    console.log(x); 
  }
  console.log(x); 
} 

exampleVar();

function exampleLet() {
  let x = 10;
  if (true) {
    let x = 20;
    console.log(x);
  }
  console.log(x); 
}

exampleLet();

```

{{< /pnote >}}

- Chaînes de caractères (strings)
- Nombres, avec ou sans décimales
- Opérateurs arithmétiques : `+ - * /`
- Booléennes : `true` ou `false` <!--représentent un valeur de vérité-->
  {{< pnote >}}

  ```js
      function boolean1() {
          return true;
        }
       

      boolean1()
  ```

  ```js
      function boolean2(a, b) {
        return a < b;
        
      }

      boolean2(10, 15)
  ```
  {{< /pnote >}}
- Liste de valeurs (arrays), par exemple : ` let todos = ['objet a', 'objet b', 'objet c'];`
- Objets : listes de clés associées à des valeurs. 
  {{< pnote >}}
  ```js
  var monObjet = {
    "klé": "valeur",
    "nom": "objet",
    "type" : "typeDeDonnees"
  }

  var nomObjet = monObjet.nom;
  ```
  {{< /pnote >}}
- `null`
- `undefined`

  {{< pnote >}}
  ```js
  let x = null;
  let y;

  console.log(x);
  console.log(y);
  ```

  {{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## Méthodes 
Une fonction se référant directement à un objet.

 Par exemple `.push()` et `.pop()` : 
 ```js
 var maListe = [1, 2, 3];
 var nouvelleListe = maListe.push(4);
 ``` 
 ```js
 var autreListe = [6, 7, 8];
 var autreNouvelleListe = autreListe.pop();
 ``` 

{{< psectiono >}}


{{< psectioni >}}
## Fonctions
JavaScript est un langage de programmation *fonctionnel*.
- Définir une fonction
- Exécuter une fonction
- Une fonction peut avoir des paramètres
- Les paramètre peuvent correspondre à des valeurs (arguments)

Par exemple :
```js
function direBonjour() {
  alert('Bonjour!');
}
direBonjour();

``` 

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## Fonctions
{{< /pcache >}}
### Les arguments
```js
function direBonjour(nom) {
  alert('Bonjour ' + nom + '!');
}

direBonjour('Alice');

``` 
{{< psectiono >}}



{{< psectioni >}}

## Atelier JavaScript
Activité réalisée par [Louis-Olivier Brassard](https://www.loupbrun.ca/)

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## Atelier JavaScript
{{< /pcache >}}
### Boucles
Une séquence d'instructions ou de codes répétée jusqu'à l'obtention d'un résultat final.
- Exemple : for 
  ```js
  console.log(`Comptons jusqu’à 3… ou 2?`);

  for (let i = 0; i < 3; i++) {
  console.log(i);
  }
  ```
 {{< pnote >}}
 ### While : Parmi les trois options proposées, laquelle permet d'obtenir le même résultat ?
  ```js
  let i = 0;
  while(i < 3); {
  i++;
  };
  console.log(i)
``` 
```js
let z = 0;
while (z < 3); {
  console.log(i);
  z++;
};
```
```js
let y = 0;
while (y < 3) {
  console.log(i)
};
```
{{< /pnote >}}
{{< psectiono >}}



{{< psectioni >}}
## Un jeu devinette en JavaScript
Ici vous pouvez télécharger le code que nous avons vu ensemble :   

- <a href="/atelier-js/4-jeu/index.html" download>HTML</a>
- <a href="/atelier-js/4-jeu/style.css" download>CSS</a>
- <a href="/atelier-js/4-jeu/script.js" download>JS</a>

Amusez vous à le personnaliser !

{{< psectiono >}}



