+++
title = "Séance 04 - Générateur de site statique (1/2)"
date = "2021-09-10"
date_p = "2023-05-09"
description = "Pour transformer des fichiers textes en page Web."
layout = "diapositive"
visible = true
saison = "3a"
+++

{{< psectioni >}}

# Plan de la séance (1/2) : 

- Produire un (petit) site web HTML avec `pandoc`
- Intégrer une bibliographie Zotero pour son CV
- [Voici un exemple](https://github.com/loup-brun/atelier-generateur-site-statique/) de ce que nous allons construire!
- Prochaine étape (séance 2/2) : déployer son site gratuitement sur GitHub Pages.

{{< psectiono >}}

{{< psectioni >}}

![Aperçu du site statique](/images/apercu-site-statique.png)

{{< psectiono >}}

{{< psectioni >}}

## Création d’une page web

Une page web, c’est un fichier HTML.

<details>
<summary>Qu’est-ce que le HTML?</summary>

On se rappelle, le HTML consiste le plus souvent en des ensembles de paires de balises ouvrantes/fermantes (par ex. `<p> </p>`). C’est le langage de documents inventé par Tim Berners-Lee pour le World Wide Web – ce système hypertexte permettant aux chercheurs du monde entier de partager leurs connaissances.

```html
<body>
  <h1>Mon titre</h1>

  <nav class="table-des-matieres">
    <a href="#section-1">Section 1</a>
    <a href="#section-2">Section 2</a>
    <a href="#section-3">Section 3</a>
  </nav>

  <p>Contenu de mon document.</p>
</body>
```

</details>

{{< psectiono >}}

{{< psectioni >}}

Il est possible de visiter la première page web : http://info.cern.ch/hypertext/WWW/TheProject.html

![Aperçu de la première page web](/images/world-wide-web-apercu.png)

{{< psectiono >}}

{{< psectioni >}}

### Pourquoi publier une page web?

Ou comprendre l’activité de publication?

{{< psectiono >}}

{{< psectioni >}}

> Enseigner l’activité de publication et en faire le pivot de l’apprentissage de l’ensemble des savoirs et des connaissances. Avec la même importance et le même soin que l’on prend, dès le cours préparatoire, à enseigner la lecture et l’écriture. […] <mark>Comprendre enfin que l’impossibilité de maîtriser un “savoir publier”, sera demain un obstacle et une inégalité aussi clivante que l’est aujourd’hui celle de la non-maîtrise de la lecture et de l’écriture, un nouvel analphabétisme numérique hélas déjà observable.</mark> […] comme le rappelait Bernard Stiegler : “la démocratie est toujours liée à un processus de publication – c’est-à-dire de rendu public – qui rend possible un espace public : alphabet, imprimerie, audiovisuel, numérique.”

— Olivier Ertzscheid, 2012, archivé sur [affordance.info](https://affordance.typepad.com/mon_weblog/2012/04/et-si-on-enseignant-vraiment-le-numerique-.html)

{{< psectiono >}}

{{< psectioni >}}

### Création d’une page HTML

Nous pourrions créer une page avec du contenu comme ceci…

```html
<!DOCTYPE html>
...
<p>
  Un premier paragraphe.
</p>

<p>
  Un deuxième paragraphe, avec un <a href="http://debugue.ecrituresnumeriques.ca">hyperlien</a>.
</p>

...

```

… mais éditer un fichier HTML à la main, c’est un peu verbeux.

_N’y aurait-il pas un autre format de balisage léger qui nous permettrait d’arriver au même résultat?_

{{< psectiono >}}


{{< psectioni >}}

### Markdown

Créons plutôt un fichier markdown, avec l’extension `.md` :

```shell
# dans le terminal, créer un fichier vide

touch page.md
```

Ajoutons un peu de contenu à ce fichier. Ouvrez `page.md` pour édition.

```markdown
# Titre de mon document

Un premier paragraphe.

1. une
#. liste
#. numérotée
#. automatiquement

```

{{< psectiono >}}


{{< psectioni >}}

### Conversion avec Pandoc

Essayons une première conversion avec les options par défaut. Pandoc reconnaîtra les formats avec les extensions de fichier (`.md` pour le markdown, `.html` pour HTML).

```shell
pandoc page.md -o page.html
```

En examinant le contenu de page.html, ne manque-t-il pas quelques balises?

Où sont passées les éléments obligatoires `<html>`, `<body>`, etc.?

On peut y remédier en spécifiant à Pandoc l’option `--standalone` (ou `-s` pour faire court).

```shell
pandoc --standalone page.md -o page.html
```

Et voilà, un fichier HTML valide!

{{< psectiono >}}


{{< psectioni >}}

### Métadonnées

Nous pouvons aller un peu plus loin en spécifiant quelques métadonnées à même le fichier :

```yaml
---
title: Titre de mon document
author: Louis-Olivier Brassard
date: 2023-05-24
---
```

Relançons la commande et voyons le résultat!

{{< psectiono >}}

{{< psectioni >}}

![Aperçu de la page HTML](/images/apercu-page-html.png)

_Aperçu de la page HTML dans le navigateur._

{{< psectiono >}}

{{< psectioni >}}

## Ajout d’une feuille de style CSS

Pandoc permet de spécifier des feuilles de style CSS à appliquer à notre document.

Créons un fichier `style.css` et ajoutons-lui quelques styles de base.

```css
/* style.css */

/* Couleur de fond et couleur de police pour l’ensemble du document */
html {
  background-color: whitesmoke;
  color: midnightblue;
}

/* Style des en-têtes */
h1, h2, h3 {
  color: deeppink;
  font-family: sans-serif;
} 
```

{{< psectiono >}}

{{< psectioni >}}

À présent, nous pouvons référencer `style.css` dans les métadonnées de notre page :

```yaml
---
css:
  - style.css
---
```

Relançons la commande Pandoc :

```shell
pandoc --standalone page.md -o page.html
```

Admirez le résultat avec votre feuille de style appliquée!

{{< psectiono >}}

{{< psectioni >}}

![Aperçu de la page HTML avec la feuille de style](/images/apercu-page-avec-css.png)

_Aperçu de la page HTML dans le navigateur._

{{< psectiono >}}

{{< psectioni >}}

## Plusieurs pages HTML : vers un premier « site »

Pour notre projet, créons une série de pages :

```shell
touch accueil.md \
      page-1.md \
      page-2.md \
      page-3.md
```

N’oublions pas d’ajouter quelques métadonnées, comme le titre, le nom de l’auteur et la date.

```yaml
---
title: Titre de la page
date: 2023-05-24
---
```

{{< psectiono >}}

{{< psectioni >}}

On peut lancer la conversion Pandoc pour chacun de ces documents :

```shell
pandoc --standalone accueil.md -o accueil.html
pandoc --standalone page-1.md -o page-1.html
pandoc --standalone page-2.md -o page-2.html
pandoc --standalone page-3.md -o page-3.html
```

Un peu fastidieux et répétitif… mais ça fonctionne! _(Nous y reviendrons plus loin.)_

{{< psectiono >}}

{{< psectioni >}}

### Ajout d’un menu

À présent que nous avons plusieurs pages, essayons d’ajouter un menu pour faciliter la navigation entre elles!

Dans l’en-tête du markdown, ajouter la propriété `include-before`, qui permet d’ajouter une liste de choses. Ajoutons un élément dans cette liste (désigné par le trait d’union `-`) en permettant la saisie sur plusieurs lignes, grâce au caractère `|` (<em lang="en">pipe</em>) :

```yaml
include-before:
  - |
    <nav class="menu">
      <a href="accueil.html" class="courante">Accueil</a>
      <a href="page-1.html">Page #1</a>
      <a href="page-2.html">Page #2</a>
      <a href="page-3.html">Page #3</a>
    </nav>

```

{{< psectiono >}}

{{< psectioni >}}

### Ajout d’un pied de page

Puisqu’il s’agit d’un site, nous pouvons ajouter du contenu HTML à la fin du document (propriété `include-after`) pour les crédits et les droits d’utilisation par exemple :

```yaml
include-after:
  - |
    <footer>
      <hr>
      &copy; 2023 – Tous droits réservés.
    </footer>

```

{{< psectiono >}}

{{< psectioni >}}

![Aperçu avec menu](/images/apercu-page-avec-menu.png)

_Aperçu d’une page avec un menu._

{{< psectiono >}}

{{< psectioni >}}

### Un petit plus pour le style…

Enfin, nous pouvons faire appel à une feuille de style externe, à tout hasard [almond.css](https://alvaromontoro.github.io/almond.css/) pour rendre notre document plus joli.

```yaml

css:
  - https://unpkg.com/almond.css@latest/dist/almond.lite.min.css
  - style.css

```

{{< psectiono >}}

{{< psectioni >}}

## Création d’un modèle HTML et utilisation de variables

C’est un peu répétitif d’indiquer les mêmes variables dans plusieurs fichiers… par exemple, si nous souhaitons changer le pied de page de 2022 pour 2023, il faudrait le faire à la main dans chaque page! (`page-1.html`, `page-2.html`, `page-3.html`, …) 

À moins que…

{{< psectiono >}}

{{< psectioni >}}

### Création d’un fichier de métadonnées unique

Nous allons référer nos variables communes dans un nouveau fichier de type [YAML](https://yaml.org/). (Vous pouvez utiliser l’extension `.yml` ou `.yaml`; assurez-vous simplement de conserver le même choix par la suite.)
N’ayez crainte, ce format correspond simplement à celui des métadonnées que vous inscriviez dans l’en-tête d’un fichier markdown.


Créez le fichier `variables.yml` :

```shell
touch variables.yml
```

{{< psectiono >}}

{{< psectioni >}}

Ouvrez le fichier `variables.yml` pour édition. Nous allons y reporter les variables répétées dans nos fichiers markdown.

```yaml
# ajout du menu dans le HTML
include-before:
  - |
    <nav class="menu">
      <a href="accueil.html">Accueil</a>
      <a href="page-1.html">Page #1</a>
      <a href="page-2.html">Page #2</a>
      <a href="page-3.html">Page #3</a>
    </nav>

# pied de page
include-after:
  - |
    <footer>
      <hr>
      &copy; 2023 – Tous droits réservés.
    </footer>

# feuille de style externe
css:
  - https://unpkg.com/almond.css@latest/dist/almond.lite.min.css
  - style.css
document-css: true # inclure les styles par défaut de pandoc

```

{{< psectiono >}}

{{< psectioni >}}

### Utiliser le fichier de métadonnées

Pour faire référence au fichier de métadonnées, il s’agit d’ajouter une option supplémentaire à la commande Pandoc, l’option `--metadata-file [fichier]` :

```shell
pandoc --metadata-file variables.yml --standalone accueil.md -o accueil.html

# et ainsi de suite pour chaque page...
```

{{< psectiono >}}

{{< psectioni >}}

## Option avancée : gérer son propre modèle HTML

Utilisez la pleine puissance des variables et des modèles! Ce principe est à la base de tous les générateurs de site web –– statiques ou non.

[Modèle HTML de base utilisé par Pandoc](https://github.com/jgm/pandoc/blob/main/data/templates/default.html5)

Créez un fichier modèle : `_modele.html`.
(Le recours à la barre en bas n’est pas obligatoire; il s’agit d’une petite convention pour distinguer les fichiers HTML à publier des fichiers HTML modèles.)

```shell
touch _modele.html
```

{{< psectiono >}}

{{< psectioni >}}

<details>
<summary><strong>Exemple de contenu pour le modèle HTML</strong></summary>

```html
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8" />

  <!-- Titre du document -->
  <title>$pagetitle$</title>

  <!-- Auteur -->
  <!-- Note: la variable `author-meta` est géré par pandoc, utilisez `author` dans vos fichiers d’écriture -->
  $for(author-meta)$
  <meta name="author" content="$author-meta$" />
  $endfor$

  <!-- Date -->
  <!-- Note: la variable `date-meta` est géré par pandoc, utilisez `date` dans vos fichiers d’écriture -->
  $if(date-meta)$
  <meta name="dcterms.date" content="$date-meta$" />
  $endif$

  <!-- Description -->
  $if(description-meta)$
  <!-- Note: la variable `description-meta` est géré par pandoc, utilisez `description` dans vos fichiers d’écriture -->
  <meta name="description" content="$description-meta$" />
  $endif$

  <!-- Styles, chemins spécifiés dans les métadonnées -->
  $for(css)$
  <link rel="stylesheet" href="$css$" />
  $endfor$

  <!-- Il est possible d’inclure d’autres éléments dans l’en-tête -->
  $for(header-includes)$
  $header-includes$
  $endfor$
</head>

<body>
<!-- Corps du document -->

$for(include-before)$
$include-before$
$endfor$

<!-- Ours (bannière, menu...) -->
<header class="ours"></header>

<!-- Disposition principale -->
<main>
  <!-- Barre latérale -->
  <div class="barre-laterale"></div>

  <!-- Contenu principal -->
  <article class="contenu-principal">
    $body$
  </article>
</main>

<!-- Pied-de-page -->
<footer class="pied-de-page"></footer>

$for(include-after)$
$include-after$
$endfor$
</body>
</html>
```

</details>

{{< psectiono >}}

{{< psectioni >}}

## Utiliser le modèle

Pour utiliser le fichier modèle avec Pandoc, utilisez l’option `--template [fichier]`.

```shell
# les barres obliques servent à écrire la même commande sur plusieurs lignes
pandoc --template _modele.html \
       --metadata-file variables.yml \
       --standalone \
       accueil.md \
       -o accueil.html
       
# ... évidemment, répéter pour chaque fichier à transformer en HTML
```

{{< psectiono >}}

{{< psectioni >}}

## Un script pour éviter la répétition

Toute cette répétition est bien fastidieuse! Ne pourrait-on pas simplifier toutes ces commandes répétitives en une seule?

Créons un fichier shell `site.sh` :

```shell
touch site.sh
```

Une première manière simple serait de retranscrire nos commandes pour chaque page que nous souhaitons convertir. Commençons par cela, le gain de temps se fera déjà sentir!

<details>
<summary><strong>Contenu du fichier <code>site.sh</code></strong></summary>

```bash
#!/bin/bash

# `echo` est utilisé pour avoir un retour de l’activité qui se déroule dans la console
echo "Production du fichier accueil.html..."
pandoc --template _modele.html \
       --metadata-file variables.yml \
       --standalone \
       accueil.md \
       -o accueil.html

echo "Production du fichier page-1.html..."
pandoc --template _modele.html \
       --metadata-file variables.yml \
       --standalone \
       page-1.md \
       -o page-1.html

# ... et ainsi de suite
```

</details>

{{< psectiono >}}

{{< psectioni >}}

C’est un peu long. On pourrait réunir les options passées à Pandoc dans une variable, `OPTIONS_PANDOC` :

```bash
OPTIONS_PANDOC="--template _modele.html --metadata-file variables.yml --standalone"
```

… et l’utiliser ainsi dans le script :

```bash
pandoc $OPTIONS_PANDOC [source] -o [sortie]
```

Lorsque vous aurez terminé les substitutions, sauvegardez votre script. Dans votre terminal, essayez de l’appeler comme ceci :

```shell
bash site.sh
```

{{< psectiono >}}

{{< psectioni >}}

Vos fichiers devraient avoir être produits!

## Pour aller un peu plus loin…

Nous avons réussi à automatiser la fabrique de nos fichiers `accueil.html`, `page-1.html`, etc.; sauf que si nous renommons nos fichiers (par exemple : `accueil.md` => `index.md`) ou que nous en créons un nouveau (`page-4.html`), notre script ne sera plus à jour! Pouvons-nous faire quelque chose?

<details>
<summary><strong>Nouveau contenu du fichier <code>site.sh</code></strong></summary>

```shell
#!/bin/bash

# Aller chercher tous les fichiers source (markdown)
SOURCE=$(find . -iname "*.md" -not -iname "README*" -maxdepth 1)
# Énoncer les contreparties HTML
HTML=$(find . -iname "*.html" -not -iname "README*" -not -iname "_modele*" -maxdepth 1)
# options pour pandoc
OPTIONS_PANDOC="--to html --standalone --metadata-file=variables.yml --template _modele.html --toc --citeproc"

function clean() {
  echo "* Nettoyage des fichiers HTML..."
  for i in $HTML; do
    echo "  rm $i"
    rm $i;
  done
  echo "" # produire une ligne vide dans la sortie de la console
}

function html() {
  cd $(pwd)

  echo "* Fabrication des fichiers HTML..."
  for i in $SOURCE; do
    echo "  Conversion de $i"
    pandoc $OPTIONS_PANDOC $i -o ${i/.md/.html};
  done;
  echo "" # produire une ligne vide dans la sortie de la console
}

function all() {
  clean
  html

  echo "Terminé!"
}

# et on lance la fonction `all` (qui fait tout)
all
```

</details>

{{< psectiono >}}

{{< psectioni >}}

En résumé, les fichiers que nous avons :

```
.
├── _modele.html
├── accueil.html
├── accueil.md
├── page-1.html
├── page-1.md
├── page-2.html
├── page-2.md
├── page-3.html
├── page-3.md
├── site.sh
├── style.css
└── variables.yml
```

{{< psectiono >}}

{{< psectioni >}}

## Un (petit) site personnel

Si vous avez réussi à parcourir toutes les étapes du tutoriel, bravo! Vous avez acquis un savoir exceptionnel qui vous permettra de mettre en place votre propre petit site web statique.

Téléchargez le projet et poursuivez votre aventure à partir du dossier `site/` qui se trouve à la racine :

https://github.com/loup-brun/atelier-generateur-site-statique

{{< psectiono >}}

{{< psectioni >}}

## D’autres outils

Il existe un grand nombre de générateurs de sites statiques. Voici une petite liste :

- [Jekyll](https://jekyllrb.com/) (écrit en Ruby)
- [Hugo](https://gohugo.io/) (écrit en Go)
- [Pelican](https://getpelican.com/) (écrit en Python)
- [Zola](https://www.getzola.org/) (similaire à Hugo, écrit en Rust)
- [Hakyll](https://jaspervdj.be/hakyll/) (écrit en Haskell)
- [Hexo](https://hexo.io/) (écrit en JavaScript)

… mais tous produisent du HTML à la fin!

Et il y en a [bien d’autres…](https://jamstack.org/generators/)

{{< psectiono >}}

{{< psectioni >}}

## Pourquoi un générateur de site statique?

- **Pages pérennes, facilement archivables** (ex. Internet Archive - archive.org)
- **Le HTML jouit d’une spécification ouverte** (Tim Berners-Lee a placé son invention dans le domaine public)
- **Des pages performantes et résilientes** puisqu’elles n’ont pas besoin d’être « traitées » (« calculées », « imprimées » ou « emballées », selon votre analogie préférée!) sur demande – elles le sont déjà!
  - Moins de vulnérabilités aux attaques groupées (<abbr lang="en" title="Distributed Denial of Service">DDoS</abbr>)
  - Pas d’injection de code par défaut! :)
- **Économique** pour des environnements de publication très peu gourmands en énergie.

{{< psectiono >}}

{{< psectioni >}}

### Cas d’usage

- Page personnelle
- Blog, index
- Documentation
- Nuage de mots-clefs
- Publication pérenne (ex. [les](http://www.softphd.com/) [thèse](https://these.arthurperret.fr/) de [doctorat](https://these.nicolassauret.net/))
  - indexable et cherchable

{{< psectiono >}}

