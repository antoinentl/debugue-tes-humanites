+++
title = "Séance 03 - Les formats des fichiers"
date = "2021-09-10"
date_p = "2023-02-14"
description = "Définitions et usages. Markdown, HTML, PDF."
layout = "diapositive"
visible = true
saison = "3b"
+++

{{< psectioni >}}
## Plan de la séance

1. Définitions
2. Les formats : docx, pdf, markdown, HTML, XML
3. La conversion entre formats
4. Pandoc
{{< psectiono >}}

{{< psectioni >}}
## 1. Définitions

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 1. Définitions
{{< /pcache >}}
### 1.1. Origines des formats

{{< pnote >}}
Partons de l'édition : les formats de papier pour l'imprimé, première apparition _technique_ du terme ?

Le terme _format_ est un terme technique, son usage permet de délimiter les caractéristiques d'un objet : avec le format nous donnons un certain nombres de données, d'instructions, ou de règles.
Pourquoi définir tout cela ?
L'objectif est de constituer une série d'informations compréhensible, utilisable et communicable.

Pour prendre un exemple concret du côté du livre, l'impression d'un document nécessite de s'accorder sur un format de papier.
Les largeurs, longueurs et orientations sont normalisées, des standards sont établis, ils permettent alors de concevoir des imprimantes qui peuvent gérer des types définis de papier.
Sans des formats de papier il est difficile de créer des machines adéquates, comme des presses à imprimer ou des imprimantes.
L'usage du format dans l'imprimerie est sans doute la première apparition de ce terme technique, il est intéressant de noter que le _format_ est ainsi d'abord attaché au livre et à sa fabrication.

Notons également que des outils ou des processus sont associés au format : les instructions sont définies pour qu'une action soit réalisée par un agent — humain, analogique, mécanique, numérique.

Enfin, sans format, pas de média.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 1. Définitions
{{< /pcache >}}
### 1.2. Qu'est-ce qu'un format informatique ?
Structurer les informations avec des spécifications techniques.

{{< pnote >}}
Un _format informatique_ est le pivot entre une organisation logique et son implémentation dans un système informatique.
Un fichier doit avoir un format, sans quoi il ne pourra être produit, transmis ou lu.
Un format informatique est le lien entre l'infrastructure et l'agent (humain ou programme) qui utilise cette infrastructure.
Le choix des formats informatiques détermine la manière dont les informations sont créés, stockées, envoyées, reçues, interprétées, affichées.
Aujourd'hui les formats prennent une place importante dans notre environnement, et leur incidence dépasse le domaine de l'informatique, leur étude a pourtant été longtemps délaissée dans le champ des médias.

Exemple du format DOC ou .doc : le logiciel Microsoft Word ne peut pas lire n'importe quel format informatique, les données doivent être structurées d'une façon précise pour que le logiciel puisse les interpréter, et ensuite les modifier, et enfin produire une nouvelle version du fichier.
Ici le format DOC a été créé pour les besoins d'un logiciel spécifique.

Dans cet exemple c'est le format informatique qui est le résultat du logiciel, mais d'autres fonctionnement sont possibles.
Par ailleurs, le format DOC est longtemps resté propriétaire (jusqu'à l'arrivée du format DOCX), ses spécifications n'étaient pas publiques et des brevets empêchaient toute initiative de développement d'un logiciel autre que Word capable de lire ou de modifier des fichiers .doc.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 1. Définitions
{{< /pcache >}}
### 1.3. Pourquoi s'intéresser aux formats ?
Comprendre les rouages du numérique.

{{< pnote >}}
Les formats informatiques ont structuré et structurent encore l'espace numérique.

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## 2. Implications techniques et politiques
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 2. Implications techniques et politiques
{{< /pcache >}}
### 2.1. L'interopérabilité
La condition du numérique : faire dialoguer les machines.

{{< pnote >}}
L'interopérabilité est un principe qui permet à plusieurs machines de dialoguer :

- en s'accordant sur des règles pour définir une série d'informations, les machines peuvent lire et écrire un fichier ;
- si ces spécifications sont clairement énoncées, il n'y a alors plus de dépendance vis-à-vis d'un logiciel spécifique ;
- la question de l'interopérabilité est intimement liée à la standardisation et à l'ouverture du format.

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 2. Implications techniques et politiques
{{< /pcache >}}
### 2.2. Ouvert ou fermé ?
Standards et licences.

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## 3. Les formats
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Les formats
{{< /pcache >}}
### 3.1. PDF
- Portable Document Format : pour afficher et imprimer les fichiers toujours de la même manière
- 1991 par Adobe
- Depuis 2008 : [ISO standard](https://www.iso.org/about-us.html) [32000-1:2008](https://opensource.adobe.com/dc-acrobat-sdk-docs/standards/pdfstandards/pdf/PDF32000_2008.pdf)
- Le fichier est structuré sur quatre niveaux 
   - Header : indique la version de la spécification PDF du fichier
   - Body : contenant les objets qui constituent le document contenu dans le dossier
   - Cross-reference Table : contenant les informations sur les objets indirects dans le fichier
   - Trailer : indiquant l'emplacement de la *cross-reference table* dans le corps du fichier.
- Dans `body`, [Chaque page est traitée individuellement](https://pdfux.com/edit-metadata-pdf/)
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Les formats
{{< /pcache >}}
### 3.2. Office Open XML (DOCX)
- 2007 par Microsoft
- .doc + x(ml)
- dossier zip contenant des fichiers xml
- permet de lire comment le fichier a été encodé
- vs `.doc`, un format binaire lisible uniquement par le logiciel Word - à partir de Word97
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Les formats
{{< /pcache >}}
### 3.3. XML
- Extensible Markup Language
- crée en 1998
- conçu pour la structuration stricte de données
- les informations sont validées par des schémas
  
Démo sur Oxygen, un éditeur de texte pour XML
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Les formats
{{< /pcache >}}
### 3.4. HTML
- Hypertext Markup Language
- créé en 1993
- conçu pour les liens entre les documents (*hyperlinks*)
- cinquième version (en 2008), s'adapte à l'évolution du Web
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Les formats
{{< /pcache >}}
### 3.5. HTML
### La structure d'un fichier HTML

```
<html>
    <head>
        <meta charset="utf-8">
        <title>Titre dans l'onglet</title>
    </head>
    <body>
        <h1>Titre</h1>
        <p>Ceci contient un paragraphe.</p>
    </body>
<html>
```
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Les formats
{{< /pcache >}}
### 3.5. Markdown
- créé en 2004
- langage léger
- lisible par les humains
[Tableau d'équivalence avec Markdown](https://en.wikipedia.org/wiki/Markdown#Examples)
{{< psectiono >}}


{{< psectioni >}}
## 4. Conversion entre formats
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Conversion entre formats
{{< /pcache >}}
### 4.1. La conversion
La conversion est la transformation de balises en d’autres balises.   

Les contenus en entrée :

- texte balisé
- métadonnées sérialisées
- bibliographie structurée


{{< pnote >}}
La conversion est un processus qui prendre un certain nombre de données en compte, et pas uniquement le _texte_ balisé.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Conversion entre formats
{{< /pcache >}}
## 4.2. Pandoc
<!--aggiungi qualche nozione in più su Pandoc e su come viene usato-->
Des logiciels permettent de convertir automatiquement les contenus, comme [Pandoc](https://en.wikipedia.org/wiki/Pandoc#Supported_file_formats) par exemple :    

- 2006, par [John MacFarlane](https://johnmacfarlane.net/)
- Pandoc est un convertisseur agnostique, il n’impose pas une pratique par rapport à un balisage.
- Mais certaines conversions sont impossibles : un format complexe est trop difficile à réduire au risque de perdre la majorité des informations, ou de ne pas savoir quoi en faire.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Conversion entre formats
{{< /pcache >}}
### 4.3. Fonctionnement de Pandoc
`programme` `fichier.entrée` optionnel=`fichier.sortie`

{{< pnote >}}
Pandoc fonctionne en ligne de commande, avec un schéma classique d'options.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Conversion entre formats
{{< /pcache >}}
### 4.4. Pandoc -examples
`pandoc mon-fichier-markdown.md`

`pandoc -f markdown -t html mon-fichier-markdown.md -o mon-fichier-html.html`

`pandoc -f markdown -t html --template=mon-modele.html mon-fichier-markdown.md -o mon-fichier-html.html`

{{< pnote >}}
La première commande convertit le fichier en HTML, le format de sortie par défaut de Pandoc

La deuxième crée un fichier `html` correspondant au fichier source

La troisième commande applique un modèle ou _template_, permettant ainsi de structurer le document d'une façon plus précise.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## 5. Découverte de Pandoc par la manipulation
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 5. Découverte de Pandoc par la manipulation
{{< /pcache >}}
## 5.1. Installation du logiciel - Première étape
- Terminal
   - Windows : Start > Windows Power Shell
      - à partir de Windows 10 : installer l'application [Ubuntu on Windows](https://apps.microsoft.com/store/detail/ubuntu-on-windows/9NBLGGH4MSV6?hl=en-ca&gl=ca&rtc=1)
   - Mac : chercher « terminal »    
   - Linux : chercher « terminal »
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 5. Découverte de Pandoc par la manipulation
{{< /pcache >}}
## 5.2. Installation du logiciel - Deuxième étape
Suivre les instructions sur cette page : [https://docs.zettlr.com/fr/installing-pandoc/](https://docs.zettlr.com/fr/installing-pandoc/)
- Pandoc
   - Windows avec application Ubuntu : `sudo apt update` --> `sudo apt install pandoc`
   - Mac
   - Linux
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 5. Découverte de Pandoc par la manipulation
{{< /pcache >}}
### 5.3. Créer un document Markdown et le convertir en HTML

- créer un document Markdown avec des niveaux de titre, une liste, une citation longue et de l'emphase (italique et gras)
- lancer la commande `pandoc mon-fichier.md` en l'adaptant 
- ouvrir le fichier HTML obtenu
{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 5. Découverte de Pandoc par la manipulation
{{< /pcache >}}
### 5.4. Ajouter des métadonnées

- ajouter des métadonnées à votre document avec un entête du type :

```
---
title: Le titre de mon document
author: Mon Nom
---
```

- convertir ce fichier Markdown en HTML puis en DOCX
- ouvrir les fichiers obtenus, que remarquez-vous ?
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 5. Découverte de Pandoc par la manipulation
{{< /pcache >}}
### 5.5. Appliquer un modèle

- créer un fichier HTML avec le code suivant :

```
<html>
    <head>
    <title>$titre$</title>
    <meta name="author" content="$auteur$">
    </head>
    <body>
    <h1 class="titre">$titre$</h1>

    <p class="auteur">$auteur$</p>

    $if(date)$
    <p class="date">$date$</p>
    $endif$


    <div>$body$</div>

    </body>
</html>
```


- lancer la conversion en HTML en appliquant le modèle/template
- ouvrir le document obtenu
{{< psectiono >}}



{{< psectioni >}}
### 5.6. Appliquer un modèle - 2
Ajouter dans le YAML du fichier Markdown le code suivant :  

```
---
titre: The document title
auteur:
- nom: Auteur 1
  affiliation: Université de Montréal
- nom: Auteur 2
  affiliation: Université de Laval
...

```
{{< psectiono >}}




{{< psectioni >}}
### 5.7. Appliquer un modèle - 2
Modifier le fichier HTML comme suit :

```
<html>
    <head>
    <title>$titre$</title>
    <meta name="author" content="$auteur$">
    </head>
    <body>
    <h1 class="titre">$titre$</h1>

    $for(auteur)$
    $if(auteur.nom)$
    <p class="author">$auteur.nom$</p>
    $if(auteur.affiliation)$ <p class="author">($auteur.affiliation$)</p>$endif$
    $else$
    <p class="author">$auteur$</p>
    $endif$
    $endfor$

    $if(date)$
    <p class="date">$date$</p>
    $endif$


    <div>$body$</div>

    </body>
</html>

```
{{< psectiono >}}


{{< psectioni >}}
## Ressources

- Arthur Perret, L’écriture académique au format texte, [https://www.arthurperret.fr/2021-09-21-ecriture-academique-format-texte.html](https://www.arthurperret.fr/2021-09-21-ecriture-academique-format-texte.html)
- Antoine Fauchié, Fabriques de publication : Pandoc, [https://www.quaternum.net/2020/04/30/fabriques-de-publication-pandoc/](https://www.quaternum.net/2020/04/30/fabriques-de-publication-pandoc/)
- Dennis Tenen et Grant Wythoff, Rédaction durable avec Pandoc et Markdown, Programming Historian, [https://programminghistorian.org/fr/lecons/redaction-durable-avec-pandoc-et-markdown](https://programminghistorian.org/fr/lecons/redaction-durable-avec-pandoc-et-markdown)
- Nicolas Sauret et Marcello Vitali-Rosati, tutorielMdPandoc, [https://framagit.org/stylo-editeur/tutorielmdpandoc](https://framagit.org/stylo-editeur/tutorielmdpandoc)
- documentation de Pandoc : [https://pandoc.org/MANUAL.html](https://pandoc.org/MANUAL.html)

{{< psectiono >}}
