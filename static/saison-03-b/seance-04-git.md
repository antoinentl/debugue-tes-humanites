+++
title = "Séance 04 - Git"
date = "2021-09-10"
date_p = "2023-03-28"
description = "Le versionnement des documents."
layout = "diapositive"
visible = true
saison = "3b"
+++
{{< psectioni >}}
{{< pcache >}}
## Plan de la séance

1. Les principes de Git
2. Installer et configurer Git
3. Les commandes les plus usuelles
4. Un peu plus avancé : conflits, merge, rebase, forks 
{{< /pcache >}}


{{< pnote >}}
Pourquoi s'évertuer à utiliser absolument Git ?
Réponse en quelques principes théoriques et avec des manipulations !
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. Qu'est-ce que Git ? Trois principes
Git est un logiciel de gestion de versions décentralisé : il met en place un _système distribué de contrôle de versions_ (Distributed version control system).
- **Système distribué (Distributed system)**
- **Versionnement**
- **Contrôle**

{{< pnote >}}
1. **Système distribué** : un ensemble de composants indépendants situés sur des machines différentes qui partagent des messages entre eux afin d'atteindre des objectifs communs.
2. **Versionnement** : chaque fois qu'on enregistre (commit), ou qu'on sauvegarde l'état de notre projet dans Git, Git prend un instantané de tous les fichiers, en sauvegardant une référence à l'instantané. Git traite donc nos fichiers comme une série d'instantanés dans le temps. Pour être plus efficace, si Git recconaît que certains fichiers n'ont pas changé depuis l'instantané précédent, Git ne les récupère pas à nouveau, mais crée smplement une collation vers le fichier précédent qui a déjà été sauvegardé. 
3. **Contrôle** : il n'est pas possible d'enregistrer (commit) une modification dans un fichier sans que cette modification soit vérifiée par le système de Git. Il ne peut pas arriver que des information soient perudes ou qu'un fichier soit corrompu sans que Git s'en aperçoive. Le mécanisme utilisé par Git pour mettre en œuvre cette vérification est un hash appelé SHA-1.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}

### 1.1. Un système distribué
- Git permet de copier localement (c'est à dire de sauver sur le disque dur de son propre ordinateur) un dépôt contenu sur un serveur tiers. 
- Possibilité d'avoir plusieurs dépôts distants
- On peut ainsi "committer" du contenu, créer des branches et les merger en local.
- `git push` pour ajouter les changement que on a fait en local sur le dépôt à distance. 
- Décentraliser pour mieux travailler

{{< pnote >}}
#### En d'autres mots :
- un système distribué permet de ne pas centraliser la sauvegarde ;
- de _cloner_ facilement = copier le dossier de travail avec l'ensemble de l'historique ;
- de travailler hors connexion ;
- de créer des versions _parallèles_ d'un projet

{{< /pnote >}}

{{< psectiono >}}

{{< psectioni >}}
### 1.2. Versionner
`mon-fichier-v2-relu-2021-12-09-final-ok-okok.txt`

Pour suivre un projet dans son ensemble (et pas forcément un fichier en particulier).

{{< pnote >}}
Le versionnement (_versioning_ en anglais) entend répondre à plusieurs objectifs :

- enregistrer un ou plusieurs fichiers
- garder une trace des versions des fichiers
- naviguer dans l'historique des versions
- (le tout à plusieurs)

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
### 1.3. Versionner : Décentraliser pour mieux travailler
Plusieurs principes inhérents à une bonne gestion collective de fichiers informatiques :

- ne pas centraliser la sauvegarde
- _cloner_ facilement = copier le dossier de travail avec l'ensemble de l'historique
- travailler hors connexion
- créer des versions _parallèles_ d'un projet -->

{{< pnote >}}
Il y a eu de nombreux systèmes ou logiciels de gestion de versions, mais tous avaient un ou plusieurs défauts :

- la nécessité d'être connecté pour travailler
- ne pas pouvoir disposer de tous les fichiers sur sa propre machine
- centraliser les fichiers à un seul endroit
- etc.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}


### 1.4. Contrôle 
Git est un système de fichiers adressables en fonction de leur contenu.

{{< pnote >}}

Chaque fichier du dépôt Git est identifié par une chaîne de caractères hexadécimaux (0-9 et a-f) qui est calculée à partir du contenu du fichier et de sa position dans le dépôt.
- cette chaîne est appelé _hash_
- un identifiant pour un commit
- ex. `24b9da6552252987aa493b52f8696cd6d3b00373`

{{< /pnote >}}

{{< psectiono >}}

{{< psectioni >}}
<!--
mkdir folder
git init .
git status
git add file.md (staging area)
git commit -m "comment"
git log
(modifier le fichier)
git status 
git diff file.md
 -->
## 2. Installation

- Linux : `sudo apt install git-all`
- Mac : `git --version`
- Windows : Téléchargez le logiciel [ICI](https://git-scm.com/download/win)

Pour voir si git est bien installé sur votre machine : `git --version`

{{< psectiono >}}


{{< psectioni >}}

### Exercice

- dans un dossier `mon-dossier`
- créer un fichier mon-fichier.md
- `git init`
- `git status`
- `git add mon-fichier.md`
- `git commit -m "le message d'enregistrement"`
- `git log`
- (`git fetch`, `git rebase` et `git push`)


{{< psectiono >}}


{{< psectioni >}}
## 3. Créer un projet en utilisant Git

### Examples

- programmes informatiques : https://github.com/gohugoio/hugo/
- documentation : https://github.com/jgm/pandoc/tree/master/doc
- projets éditoriaux : https://github.com/thegetty/romanmosaics


{{< psectiono >}}



<!--{{< psectioni >}}
## 1. Les principes de Git

- suivre un projet
- développements/évolutions non linéaires (branches)
- historique général et particulier
- système de gestion de versions distribué
- complexité relative
- logiciels/plateformes qui simplifient l'usage

{{< pnote >}}
Git est pensé pour versionner **un projet**, seul·e ou à plusieurs.
Si Git permet de versionner des fichiers, il ne faut pas oublier que l'objectif final est bien de suivre un projet dans son ensemble et pas forcément un fichier en particulier
{{< /pnote >}}
{{< psectiono >}} -->


{{< psectioni >}}
### Quelques définitions

- **dépôt** : ensemble des fichiers versionnés
- **commit** : enregistrement des changements dans un dépôt
- **fork** : divergence d'un projet, une copie qui va vivre sa propre vie (ou presque)
- **cloner** : copier un projet chez soi pour pouvoir y contribuer
- **conflit** : lorsque l'on tente de réunir voir de _fusionner_ plusieurs versions d'un même projet

{{< pnote >}}
Un commit est une série d'informations :

- un identifiant
- une date
- un message
- une liste de modifications associées : les fichiers modifiés

Pour comprendre comment fonctionne Git il faut comprendre ce qu'est un _commit_ : ce n'est pas un enregistrement classique, mais l'état du projet tout entier après une série de modifications sur un ou plusieurs fichiers.

Si le _fork_ n'est pas toujours habituel (c'est la création d'une copie d'un projet qui pourra être indépendant de la version d'origine), le fait de _cloner_ un dépôt est beaucoup plus fréquent.
La différence entre les deux est aussi une différence de pratique :

- en _forkant_ on s'approprie le projet, si besoin on pourra soumettre des modifications à la _version originale_ tout en travaillant sur _notre version_ ;
- en _clonant_ le projet je conserve le projet original, mais je risque de perturber les développements.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
### Pour commencer


- bien configurer Git
- Git ≠ GitHub/GitLab
- la bible : [https://git-scm.com/book/fr/v2](https://git-scm.com/book/fr/v2)
- il est presque impossible de supprimer quelque chose

{{< pnote >}}
Comme beaucoup d'outils, Git nécessite quelques réglages préalables !
Il est possible de ajouter des configurations spécifiques pour un projet en particulier. 
{{< /pnote >}}
{{< psectiono >}}


<!--{{< psectioni >}}
## 4. Configurer Git

- configuration générale : fonctionnement de Git pour tous les projets
- configuration particulière : pour un projet
- clés SSH ?…

{{< psectiono >}}-->


{{< psectioni >}}
{{< pcache >}}
## 4. Configurer Git
{{< /pcache >}}
### 4.1. Configuration générale

- basique : un nom et une adresse
- avancée : éditeur par défaut, branche par défaut
- voir la configuration : `git config --list`
- modifier la configuration, exemple : `git config --global user.name "Prénom Nom"` ; `git config --global user.email "mon.addresse@umontreal.ca"`

<!--
mkdir folder
git init .
git status
git add file.md (staging area)
git commit -m "comment"
git log
(modifier le fichier)
git status 
git diff file.md
 -->

{{< pnote >}}
Git repose sur la reconnaissance des personnes qui contribuent ensemble à un même projet, il est donc primordial de bien identifier qui est qui.
Pour cela il faut au moins une adresse électronique, mais avec un nom ou un pseudonyme on gagne en lisibilité.

Il est aussi possible de spécifier [beaucoup d'autres paramètres](https://git-scm.com/docs/git-config), comme l'éditeur de texte par défaut qui sera utilisé pour les messages des _merges_. Par exemple : `git config --global core.editor /usr/bin/vim` (sur Windows il faut préciser le chemin complet de l'éditeur de text)
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Configurer Git
{{< /pcache >}}
### 4.2. Configuration particulière

<!--- par défaut : configuration générale qui s'applique
- a minima : les branches disponibles en local et le dépôt distant
- possible de spécifier -->

{{< pnote >}}
Il est par exemple possible de spécifier un utilisateur différent pour chaque dépôt/projet.
Cela peut être pratique pour distinguer des usages professionnels et des usages plus personnels.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Configurer Git
{{< /pcache >}}
### 4.3. Clés SSH
- Secure Socket Shell
- Protocole de cryptage
- Pour faire interagir mon ordinateur (client) avec un serveur en sécurité
- Une clé privée et une clé publique
[Source](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)
{{< pnote >}}
**N.B. :** Nous nous concentrons sur SSH, mais Git peut en réalité utiliser quatre protocoles distincts pour transférer des données : 
- Local
- HTTP
- Secure Shell (SSH) 
- Git

{{< /pnote >}}

{{< psectiono >}}

{{< psectioni >}}

## 5. Comment installer une clé SSH
- Plusieurs algorithmes pour générer une clé SSH. 
{{< pnote >}}
  - Ces algorithmes correspondent à différents modèles de cryptage de la clé.
{{< /pnote >}}
- RSA et ed25519 sont les algorithmes les plus utilisés pour générer des clés SSH. 
- Une clé publique (qui doit être chargé sur mon ordinateur) et une clé privée (qui reste en local)
- command pour générer une clé avec algorithm ed25519 : `ssh-keygen -t ed25519 -C "email@exemple.com"`
- command pour générer une clé avec l'algorithme RSA : `ssh-keygen -t rsa -b 4096 -C "email@exemple.com"`
- `-t` pour spécifier l'algorithme à utiliser
- `-b` pour spécifier la longer (bits) de la clé
- `-C` pour ajouter un commentaire
[Source](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#generating-a-new-ssh-key)

{{< psectiono >}}

{{< psectioni >}}
## 6. Les commandes les plus usuelles

- `git init` : initialiser un dossier
- `git status` : voir l'état du projet
- `git log` : afficher l'historique de la branche actuelle
- `git add` : ajouter un fichier dans l'index avant de commiter
- `git commit` : déclarer des modifications
- `git branch` : créer une nouvelle branche
- `git checkout` : pour basculer sur une branche
- `git push` : envoyer les modifications sur un dépôt distant
- `git fetch` : récupérer les modifications d'un dépôt distant
- `git pull` : récupérer les modifications d'un dépôt distant **et** les fusionner avec le dépôt local

{{< pnote >}}
Pour afficher un historique plus détaillé : `git log --decorate=full --raw`
Ou pour afficher l'arbre des branches : `git log --branches --remotes --tags --graph --oneline --decorate --pretty=format:"%h - %ar - %s"`

Le concept de branche est un raccourci bien pratique : différentes versions d'un même projet.
{{< /pnote >}}
{{< psectiono >}} 

{{< psectioni >}} 
### 6.1. Encore des fonctionnalités Git
{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}

{{< pnote >}}
{{< psectioni >}}
{{< imageg src="git-for-humans.png" >}}
{{< psectiono >}}
{{< /pnote >}}

{{< psectioni >}}
{{< imageg src="git-purr-01.jpg" >}}
{{< psectiono >}}


{{< psectioni >}}
{{< imageg src="git-purr-02.jpg" >}}
{{< psectiono >}}


{{< psectioni >}}
{{< imageg src="git-purr-03.jpg" >}}
{{< psectiono >}}



{{< psectioni >}}

{{< imageg src="basic-branching-6.png" >}}

{{< psectiono >}}


{{< psectioni >}}
### 6.2. Exercice

1. créer un dossier : `mkdir mon-git`
2. aller dans ce dossier : `cd mon-git`
3. `git init` 
4. créer un fichier : `echo "Mon texte" >fichier-01.txt`
5. sauvegarder les changements dans Git
6. afficher l'historique

{{< psectiono >}}


{{< psectioni >}}
## 7. Conflits, merge, rebase, forks 

- un conflit : il faut choisir
- forks : comment maintenir sa version à jour ?
- rebase : les bonnes pratiques difficiles à mettre en place

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.1. Un conflit : choisir/arbitrer

- pour distinguer les portions : des signes typographiques sont utilisés
- certains éditeurs de texte facilitent la visualisation des conflits
- si le conflit n'est pas résolu : impossible de continuer

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.2. Exercice : gestion d'un conflit
Suite à l'exercice 1, nous allons créer plusieurs modifications :

1. créer une nouvelle branche appelée `modifs` : `git branch modifs` et puis `git checkout modifs`
2. vous êtes désormais sur cette nouvelle branche
3. modifier le fichier, par exemple : `echo "Autre texte hop là" >fichier-01.txt`
4. enregistrer vos modifications dans Git : `git commit -a -m "révision de la première ligne"`
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.3. Exercice pt 2 : gestion d'un conflit

5. retourner sur la branche principale : `git checkout master` (ou `git checkout main` selon votre configuration)
6. effectuer une nouvelle modification : `echo "Ceci est mon texte" >fichier-01.txt`
7. enregistrer vos modifications : `git commit -a -m "réécriture"`
8. tenter de fusionner les deux branches : `git merge modifs`

Vous devez avoir un conflit !

{{< pnote >}}
Vous devez ouvrir le fichier pour résoudre le conflit :

- dans des éditeurs de texte comme Vim, Nano ou autre, Git ajoute des indications :
    - les `<<<<<<`, les `======` et les `>>>>>>`, ainsi que les mentions des commits et de `HEAD`
		- c'est à vous de conserver manuellement ce qui vous intéresse
- dans un éditeur de texte plus sophistiqué comme VSCode/VSCodium, des options vont vous être proposées pour choisir la version souhaitée (taper `code nom-du-fichier.md` sur terminal pour ouvrir VSCode en cas de conflit) ; 
- vous pouvez également utiliser des logiciels appelées "gitmerge tools", notamment meld (pour Linux : `sudo apt install meld` et `git config --global merge.tool meld`, puis `git meld` en cas de conflits)
- une fois ces modifications faites, vous devez enregistrer (`git add`) le fichier, et commiter tout cela ;
- si vous arrivez pas à résoudre les conflits et vous voulez retourner à la situation précédente : `git merge --abort`
- si vous volez annuler plusieurs commits : `git reset --hard hash-du-commit-que-je-veux-sauvegarder`
- effectuer un `git status` pour s'assurer que tout est en ordre !

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.4. Git merge
{{< imageg src="merge.png" >}}

[Source](https://git-scm.com/docs/git-merge)
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.5. Git rebase
{{< imageg src="rebase.png" >}}

[Source](https://git-scm.com/docs/git-rebase)

{{< psectiono >}}

{{< psectioni >}}

{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
- `git checkout -b modifs` pour se positionner sur une nouvelle branche secondaire
- ajoutez et commitez deux fichiers sur cette branche
- `git checkout master`
- ajoutez et commitez un fichier sur cette branche
- `git checkout modifs`
- `git rebase master` : les commits dans la branche modifs sont positionnés à la suite des commits de la branche master
- `git checkout master`
- `git rebase feature` : les commits en plus dans ma branche _modifs_ sont désormais ajoutez dans la branche master  
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.6. forks : comment maintenir sa version à jour ?

1. créer un fork du projet sur votre GitHub
2. cloner le dépôt sur votre ordinateur (dans un autre dossier que le projet précédent)
3. ajouter l'indication du fork en local : `git remote add upstream git@gitlab.chemin-du/projet`
4. pour récupérer les modifications du dépôt d'origine : `git fetch upstream`
5. pour synchroniser ces modifications avec _votre_ dépôt : `git rebase upstream/master`


{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.1. Rebase : les bonnes pratiques difficiles à mettre en place
Un tutoriel complexe à explorer si vous êtes motivé·e :

[https://git-rebase.io](https://git-rebase.io)

{{< psectiono >}}

{{< psectioni >}}

## 8. Les hosts d logiciels Git (GitLab / GitHab / FramaGit) 
- Créez un compte GitHub
- Accédez au dépôt 'debugue-un-test'.
- Explorez l'interface : Qu'observez-vous de différent par rapport aux propriétés du logiciel Git ? Qu'est-ce qui est similaire ?
- Essayez d'ajouter / de modifier les documents présents : attention aux conflits !

{{< psectiono >}}



