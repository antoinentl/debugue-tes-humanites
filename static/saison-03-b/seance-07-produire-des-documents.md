+++
title = "Séance 07 - Produire des documents"
date = "2021-09-10"
date_p = "2023-06-13"
description = "Stylo, Zettlr, Pandoc. Comment convertir des fichiers en différents formats."
layout = "diapositive"
visible = false
saison = "3b"
cancelled = true
+++
